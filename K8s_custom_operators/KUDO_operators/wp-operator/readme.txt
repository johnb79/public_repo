Sample PoC KUDO operator which creates multiple instances of an application

Tested locally, mysql-pass secret has to be created first.



After installing KUDO


Working with KUDO
# (start kudo manager and crds in the cluster)
1) kubectl kudo init    

# (install the instance of the operator)
## (this is your application as described in the operator)
2) kubectl kudo install operator/  

# (verify the Operator before install or upgrade)
3) kubectl kudo package verify operator/

# (add steps,tasks or change param values. When ready, upgrade operatorVersion: and run kubectl kudo upgrade)
## (adding phases will cause fatal error:  missing phase status: )
4) kubectl kudo upgrade operator/ --instance instance_name  

# (triggering the plan is an idempotent task, think of it as a desired state verification)
## (this is not an upgrade, controller knows nothing about the new resources. It will only verify and recreate if missing, the existing ones)
5) kubectl kudo plan trigger --name deploy --instance instance_name

# (removing the instance will remove all the resources related to the instance)
## (this doesnt remove the custom operators, kubectl get operatorversions can show you the exisiting versions)
6) kubectl kudo uninstall --instance instance_name

Delete:  kubectl kudo init --dry-run --output yaml | kubectl delete -f -


Create operator: mkdir your_folder >> cd your_folder >> 
                 kubectl kudo package new first-operator >>
                 kubectl kudo package add plan   # (this command will create the template folder)  
