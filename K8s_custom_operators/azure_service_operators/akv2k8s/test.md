helm install azure-key-vault-controller \
  spv-charts/azure-key-vault-controller \
   --namespace akv2k8s \
   --set keyVault.customAuth.enabled=true \
   --set env.AZURE_TENANT_ID=tenant_id \
   --set env.AZURE_CLIENT_ID=client_id \
   --set env.AZURE_CLIENT_SECRET=client_secret

helm install azure-key-vault-env-injector \
  spv-charts/azure-key-vault-env-injector \
  --namespace akv2k8s \
  --set installCrd=false \
  --set keyVault.customAuth.enabled=true \
  --set env.AZURE_TENANT_ID=tenant_id \  # tenant
  --set env.AZURE_CLIENT_ID=client_id \  # appId
  --set env.AZURE_CLIENT_SECRET=client_secret   # password
