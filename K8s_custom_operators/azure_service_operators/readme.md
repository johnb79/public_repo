# Install

1) Install akv2k8s
2) Install CertManager: `kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.1/cert-manager-legacy.crds.yaml`
3) Install Azure Service Operator: `helm repo add azureserviceoperator https://raw.githubusercontent.com/Azure/azure-service-operator/master/charts`
  helm upgrade --install aso https://github.com/Azure/azure-service-operator/raw/master/charts/azure-service-operator-0.1.0.tgz \
        --create-namespace \
        --namespace=azureoperator-system \
        --set azureSubscriptionID=$AZURE_SUBSCRIPTION_ID \
        --set azureTenantID=$AZURE_TENANT_ID \
        --set azureClientID=$AZURE_CLIENT_ID \
        --set azureClientSecret=$AZURE_CLIENT_SECRET \
        --set image.repository="mcr.microsoft.com/k8s/azureserviceoperator:latest"

This is a PoC with Azure Service Operator and akv2k8s.
ASO is used for creating azure resources on demand and akv2k8s for fetching secrets from azure keyvault and store them as K8s secrets.

kubectl apply -f does the magic ;)
