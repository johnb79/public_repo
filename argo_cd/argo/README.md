# ArgoCD

Demonstration of ArgoCD and Git Ops approach.

Architecture:
3 microservices (mysql, redis, wp), each one configured with a helm chart.
A seperate Helm chart (wp-app) deploys all three charts as one application.

This is ArgoCD App of Apps deployment scenario.

Main concept:
Fetch the values of secrets from a key vault --> Use ArgoCD to deploy Mysql, Redis and WP in a specific order.

app folder contains the 3 services

development and production folders are used for deployment with ArgoCD

In this case, Azure KeyVault is used.

Install:

a) Get Azure keyvault values: <https://akv2k8s.io/>

b) Install ArgoCD

c) git clone

d) cd ~/argo/development

e) kubectl apply -f argo_config/

f) check your ArgoCD UI.

Testing ArgoCD
#####################################################################################
Hooks are ways to run scripts before, during, and after a Sync operation.

```yaml
  annotations:
    argocd.argoproj.io/hook: PreSync
```

<https://argoproj.github.io/argo-cd/user-guide/resource_hooks/#resource-hooks>

#####################################################################################
Waves are usefull for Desired State, they behave like steps.

```yaml
  apiVersion: argoproj.io/v1alpha1
  kind: Application
  metadata:
    annotations:
      argocd.argoproj.io/sync-wave: "-1"
    name: mysql
    namespace: argocd
```

<https://argoproj.github.io/argo-cd/user-guide/sync-waves/#how-do-i-configure-waves>

All resources are created under wave 0 (zero)
So, if you want to execute mysql and redis installation before wordpress,
you just need to add an annotation of wave -1 to both of them.
Argo will first deploy mysql and redis and only after succesfull deployment of them will
deploy wordpress.

#####################################################################################
Application Sync Policy:
auto-sync will update resources only if there is a change on git repo. It checks every 3 minutes
self-heal will recreate any missing resources, real time.
