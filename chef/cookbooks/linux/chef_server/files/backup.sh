#!/bin/bash


backup_dir=/var/opt/chef-backup/

retention_days=1




chef-server-ctl reconfigure

chef-server-ctl backup -y

find ${backup_dir}/ -maxdepth 1 -type f -mtime +${retention_days} -exec rm -f {} \;

cp /var/opt/chef-backup/*.tgz /tmp

# create the script  touch /opt/chef_backup.sh
# change file permissions  chmod 775 /opt/chef_backup.sh
# cron schedule   crontab -e -->   0 2 * * * /opt/chef_backup.sh   --> run everyday at 2am
