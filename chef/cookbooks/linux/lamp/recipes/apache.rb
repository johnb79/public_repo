package 'apache2' do
  action :install
end

service 'apache2' do
  supports status: true
  action %i(enable start)
end
