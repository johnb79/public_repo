# install php.
package 'php5' do
  action :install
end

package 'php-pear' do
  action :install
end

package 'libapache2-mod-php5' do
  action :install
  notifies :restart, 'service[apache2]'
end

# Install php-mysql.
package 'php5-mysql' do
  action :install
  notifies :restart, 'service[apache2]'
end
