# Cookbook:: ubuntu
# Recipe:: default

#
# Copyright:: 2018, The Authors, All Rights Reserved.

# to do : - ["forwarded_port", {guest: 8080, host: 8080}]

package 'apache2' do
  action :install
end

service 'apache2' do
  action %i(enable start)
end

execute 'a2enmod_cgi' do
  command 'sudo a2enmod cgi'
  action :nothing
end

template '/usr/lib/cgi-bin/get_data.sh' do
  source 'get_data.erb'
  owner 'vagrant'
  group 'vagrant'
  mode '0755'
  action :create
  notifies :run, 'execute[a2enmod_cgi]', :immediately
  notifies :restart, 'service[apache2]', :immediately
end

template '/usr/lib/cgi-bin/get_system_info.sh' do
  source 'get_system_info.erb'
  owner 'vagrant'
  group 'vagrant'
  mode '0755'
  action :create
  #  notifies :run, 'execute[a2enmod_cgi]', :immediately
  #  notifies :restart, 'service[apache2]', :immediately
end

template '/usr/lib/cgi-bin/delay_check.sh' do
  source 'delay_check.erb'
  owner 'vagrant'
  group 'vagrant'
  mode '0755'
  action :create
  #  notifies :run, 'execute[a2enmod_cgi]', :immediately
  #  notifies :restart, 'service[apache2]', :immediately
end

template '/usr/lib/cgi-bin/cp.sh' do
  source 'cp.erb'
  owner 'vagrant'
  group 'vagrant'
  mode '0755'
  action :create
  #  notifies :run, 'execute[a2enmod_cgi]', :immediately
  #  notifies :restart, 'service[apache2]', :immediately
end

template '/usr/lib/cgi-bin/test.sh' do
  source 'test.erb'
  owner 'vagrant'
  group 'vagrant'
  mode '0755'
  action :create
  #  notifies :run, 'execute[a2enmod_cgi]', :immediately
  #  notifies :restart, 'service[apache2]', :immediately
end

template '/usr/lib/cgi-bin/submit_value.sh' do
  source 'submit_value.erb'
  owner 'vagrant'
  group 'vagrant'
  mode '0755'
  action :create
  #  notifies :run, 'execute[a2enmod_cgi]', :immediately
  #  notifies :restart, 'service[apache2]', :immediately
end
