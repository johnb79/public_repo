# frozen_string_literal: true

passwords = data_bag_item('passwords', 'mysql')

########  Configure the MySQL server.  ########
package 'mysql-server' do
  action :install
  notifies :run, 'execute[set mysql root password]', :immediate
  notifies :run, 'execute[mysql create database]', :delayed
end

execute 'set mysql root password' do
  action :nothing
  command "mysqladmin -u root password '#{passwords['root_password']}'"
  notifies :stop, 'service[mysql]', :immediate
end

service 'mysql' do
  supports status: true
  action %i[enable start]
end

execute 'mysql create database' do
  command "mysql -u root -p#{passwords['root_password']} <<EOF
  CREATE DATABASE wpdatabase;
  CREATE USER wpuser@localhost IDENTIFIED BY 'wppass';
  GRANT ALL PRIVILEGES ON wpdatabase.* TO wpuser@localhost;
  FLUSH PRIVILEGES;
  exit
EOF"
  action :nothing
end
