# frozen_string_literal: true

package 'wordpress' do
  version '4.1+dfsg-1+deb8u18'
  action :install
end

directory '/var/www/html/wordpress' do
  owner 'root'
  group 'root'
  mode '0755'
  recursive true
  action :create
  notifies :run, 'execute[copy wp in apache directory]', :delayed
  notifies :create, 'template[/var/www/html/wordpress/wp-config.php]', :delayed
end

execute 'copy wp in apache directory' do
  command 'cp -R /usr/share/wordpress /var/www/html/'
  action :nothing
end

template '/var/www/html/wordpress/wp-config.php' do
  source 'wp_config.php.erb'
  owner 'root'
  group 'root'
  mode '0755'
  action :nothing
end
