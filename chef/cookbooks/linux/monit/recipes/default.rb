#
# Cookbook:: monit
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

execute 'update os' do
  command 'apt-get update; apt-get upgrade -y'
  action :nothing
end

package 'monit' do
  action :install
  notifies :run, 'execute[update os]', :immediately
end
service 'monit' do
  supports status: true
  action [:enable, :start]
end
template '/etc/monit/monitrc' do
  source 'monit_config.erb'
  owner 'root'
  group 'root'
  mode '0600'
  action :create
  notifies :restart, 'service[monit]', :immediately
end
