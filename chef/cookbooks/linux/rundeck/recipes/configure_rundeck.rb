template '/etc/rundeck/rundeck-config.properties' do
  source 'rundeck_config_properties.erb'
  variables(
    server_url: node['ipaddress'].to_s
  )
  owner 'rundeck'
  group 'rundeck'
  mode '0755'
end

template '/etc/rundeck/framework.properties' do
  source 'rundeck_framework_properties.erb'
  variables(
    server_ip: node['ipaddress'].to_s
  )
  owner 'rundeck'
  group 'rundeck'
  mode '0755'
end

cookbook_file '/etc/rundeck/realm.properties' do
  source 'rundeck_users.txt'
  owner 'rundeck'
  group 'rundeck'
  mode '0644'
  action :create
  notifies :run, 'execute[create ssl]', :immediately
  notifies :run, 'execute[copy to truststore]', :immediately
end

execute 'create ssl' do
  command 'keytool -keystore /etc/rundeck/ssl/keystore -alias rundeck -genkey -keyalg RSA -keypass adminadmin -storepass adminadmin  <<!
            Rundeck.local
            devops
            US
            London
            London
            UK
            yes
            !'
  action :nothing
end

execute 'copy to truststore' do
  command 'cp /etc/rundeck/ssl/keystore /etc/rundeck/ssl/truststore'
  action :nothing
end

cookbook_file '/etc/default/rundeckd' do
  source 'enable_ssl.md'
  owner 'rundeck'
  group 'rundeck'
  mode '0644'
  action :create
  notifies :restart, 'service[rundeckd]', :immediately
end
