cookbook_file '/etc/apt/sources.list' do
  source 'repo_list.md'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
  notifies :run, 'execute[apt-get update]', :immediately
end

execute 'apt-get update' do
  command 'apt-get update -y; apt-get upgrade -y'
  action :nothing
end

package 'openjdk-8-jdk' do
  options '-t stretch -y'
  action :install
end

remote_file '/usr/src/rundeck_3.0.8.20181029-1.201810292220_all.deb' do
  source 'https://dl.bintray.com/rundeck/rundeck-deb/rundeck_3.0.8.20181029-1.201810292220_all.deb'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  not_if { ::File.exist?('/usr/src/rundeck_3.0.8.20181029-1.201810292220_all.deb') }
end

package 'uuid-runtime' do
  action :install
end
dpkg_package 'Rundeck' do
  source '/usr/src/rundeck_3.0.8.20181029-1.201810292220_all.deb'
  action :install
end
service 'rundeckd' do
  supports status: true
  action %i[enable start]
end
