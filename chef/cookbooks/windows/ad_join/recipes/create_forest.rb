#
# Cookbook:: ad_x_active_directory
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.
ad_config = data_bag_item('domain_join', 'admin')

powershell_script 'Install NuGet' do
  code 'Install-PackageProvider nuget -force'
  only_if '!(Get-PackageProvider NuGet -ListAvailable)'
end
powershell_script 'Install xActiveDirectory' do
  code 'Install-Module -Name xActiveDirectory -SkipPublisherCheck -Force'
  only_if '!(Get-Module xActiveDirectory -ListAvailable)'
end
dsc_resource 'AD-Domain-Services' do
  resource :WindowsFeature
  property :Name, 'AD-Domain-Services'
  property :IncludeAllSubFeature, true
  property :Ensure, 'Present'
end

dsc_resource 'Create New Domain' do
  resource :xADDomain
  property :DomainName, (ad_config['domain_fqdn']).to_s
  property :DomainAdministratorCredential, ps_credential((ad_config['domain_admin']).to_s, (ad_config['domain_admin_password']).to_s)
  property :SafemodeAdministratorPassword, ps_credential((ad_config['administrator_password']).to_s)
  property :DatabasePath, 'C:\Windows\NTDS\DATABASE'
  property :DomainMode, 'Win2012R2'
  property :DomainNetbiosName, (ad_config['domain_name']).to_s
  property :ForestMode, 'Win2012R2'
  property :LogPath, 'C:\Windows\NTDS\LOGS'
  property :SysvolPath, 'C:\Windows\NTDS\SYSVOL'
  notifies :reboot_now, 'reboot[now]', :immediately
end
reboot 'now' do
  action :nothing
  reason 'Reboot is manual only'
end
