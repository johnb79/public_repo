#
# Cookbook:: ad_join
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.
ad_config = data_bag_item('domain_join', 'admin')
# Change node DNS settings
dns_primary_ip = '192.168.0.200'
dns_secondary_ip = '8.8.8.8'

powershell_script 'set Dns' do
  code <<-EOH
  Get-DnsClient | Set-DnsClientServerAddress -ServerAddresses ("#{dns_primary_ip}", "#{dns_secondary_ip}")
  EOH
  guard_interpreter :powershell_script
  not_if "(Get-DnsClientServerAddress -AddressFamily IPv4 | ? {$_.ServerAddresses -contains \'#{dns_primary_ip}\'}).count -ne 0"
end
powershell_script 'join domain' do
  code <<-EOH
  $Username = "#{ad_config['administrator_user']}"
  $Password = "#{ad_config['administrator_password']}"
  $pass = ConvertTo-SecureString -AsPlainText $Password -Force
  $SecureString = $pass
  $MySecureCreds = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $Username,$SecureString
  Add-Computer -DomainName "#{ad_config['domain_fqdn']}" -Credential $MySecureCreds
  EOH
  guard_interpreter :powershell_script
  not_if <<-EOH
  $ComputerSystem = gwmi win32_computersystem
      $ComputerSystem.partofdomain = $True
  EOH
end

powershell_script 'Disable Server Manager auto-start' do
  code 'Get-ScheduledTask -TaskPath "\\Microsoft\\Windows\\Server Manager\\" -TaskName "ServerManager" | Disable-ScheduledTask'
  guard_interpreter :powershell_script
  not_if <<-EOH
    (Get-ScheduledTask -TaskPath "\\Microsoft\\Windows\\Server Manager\\" -TaskName "ServerManager").State -ne 'Ready'
  EOH
end
