#
# Cookbook:: ad_add_domain_controller
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.
ad_config = data_bag_item('domain_join', 'admin')
dns_primary_ip = '192.168.0.200'
dns_secondary_ip = '8.8.8.8'

powershell_script 'set Dns' do
  code <<-EOH
  Get-DnsClient | Set-DnsClientServerAddress -ServerAddresses ("#{dns_primary_ip}", "#{dns_secondary_ip}")
  EOH
  guard_interpreter :powershell_script
  not_if "(Get-DnsClientServerAddress -AddressFamily IPv4 | ? {$_.ServerAddresses -contains \'#{dns_primary_ip}\'}).count -ne 0"
end

powershell_script 'Install NuGet' do
  code 'Install-PackageProvider nuget -force'
  only_if '!(Get-PackageProvider NuGet -ListAvailable)'
end
powershell_script 'Install xActiveDirectory' do
  code 'Install-Module -Name xActiveDirectory -SkipPublisherCheck -Force'
  only_if '!(Get-Module xActiveDirectory -ListAvailable)'
end

dsc_resource 'AD-Domain-Services' do
  resource :WindowsFeature
  property :Name, 'AD-Domain-Services'
  property :IncludeAllSubFeature, true
  property :Ensure, 'Present'
end

reboot 'Restart Computer' do
  action :nothing
  reason 'Reboot is manual only'
  subscribes :cancel, 'dsc_resource[Create Domain Controller]', :delayed
end

dsc_resource 'Create Domain Controller' do
  resource :xADDomainController
  property :DomainName, 'londonsite'
  property :DomainAdministratorCredential, ps_credential("#{ad_config['domain_name']}\\#{ad_config['administrator_user']}", (ad_config['administrator_password']).to_s)
  property :SafemodeAdministratorPassword, ps_credential((ad_config['administrator_password']).to_s)
  property :DatabasePath, 'C:\Windows\NTDS\DATABASE'
  property :LogPath, 'C:\Windows\NTDS\LOGS'
  property :SysvolPath, 'C:\Windows\NTDS\SYSVOL'
end
