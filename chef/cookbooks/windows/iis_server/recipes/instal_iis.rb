#
# Cookbook:: iis_server
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

dsc_resource 'Install IIS Server' do
  resource :windowsfeature
  property :Name, 'Web-Server'
  property :Ensure, 'Present'
  property :IncludeAllSubFeature, true
end

service 'W3SVC' do
  action %i[enable start]
end
