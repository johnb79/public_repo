#
# Cookbook:: rds_server
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

[
  'RDS-Connection-Broker',
  'RDS-Gateway',
  'RDS-Licensing',
  'RDS-RD-Server',
  'RDS-Web-Access'
].each do |feature|
  dsc_resource 'Install RDS features' do
    resource :WindowsFeature
    property :Name, feature
    property :Ensure, 'Present'
  end
end
