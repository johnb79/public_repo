name 'sql_server'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'All Rights Reserved'
description 'Installs/Configures sql_server'
long_description 'Installs/Configures sql_server'
version '0.1.1'
chef_version '>= 12.14' if respond_to?(:chef_version)
