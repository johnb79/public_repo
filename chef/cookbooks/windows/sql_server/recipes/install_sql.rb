#
# Cookbook:: sql_server
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.
ad_config = data_bag_item('domain_join', 'admin')

powershell_script 'Install NuGet' do
  code 'Install-PackageProvider nuget -force'
  only_if '!(Get-PackageProvider NuGet -ListAvailable)'
end
powershell_script 'Install PowerShellGet' do
  code 'Install-PackageProvider PowerShellGet -force'
  only_if '!(Get-PackageProvider PowerShellGet -ListAvailable)'
end
