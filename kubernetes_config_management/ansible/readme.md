# Kubernetes Management

This is a PoC project which demonstrates the end-state configuration of Kubernetes with Ansible.

Purpose of PoC is to present real life code examples, showing the configuration structure at a glance.
Comparing code sample configurations (Ansible, Terraform, Helm, Yaml), trying to find the "sweet spot" (easy to read/easy to use) in Kubernetes Configuration Management

Download and test locally using ansible:
git clone

from inside the project's root folder: ansible-playbook main.yaml
in main.yaml set the state to present or absent

Comparing Kubernetes configuration tools and practices, trying to solve(?) the Kubernetes Configuration Management: An Unsolved Problem
<https://blog.argoproj.io/the-state-of-kubernetes-configuration-management-d8b06c1205>
