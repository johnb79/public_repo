# Header

components.yaml is K8s metric server, ready to be deployed in the local docker-desktop kubernetes.
Metric server is required if you need to test horizontal pod autoscaler. Metric server is not installed by default.

script.js is the config file for the load testing tool, K6.
link: <https://k6.io/>
