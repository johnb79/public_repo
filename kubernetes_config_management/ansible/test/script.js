import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
    stages: [
        //{ duration: '30s', target: 20 },
        //{ duration: '1m', target: 300 },
        { duration: '6m', target: 200 },
    ],
};

export default function () {
    let res = http.get('http://10.105.150.118:80/sample-page');
    check(res, { 'status was 200': r => r.status == 200 });
    sleep(1);
}


// * Load testing using K6  : https://k6.io

// ? if tou want to run your tests inside a container:
// ? from inside the script directory: docker run -i loadimpact/k6 run - <script.js
// ? https://k6.io/docs/getting-started/running-k6#stages-ramping-up-down-vus