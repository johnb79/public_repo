# Use Azure Key Vault with Kubernetes

Install Helm and Secrets Store CSI Driver
<https://docs.microsoft.com/en-us/azure/key-vault/general/key-vault-integrate-kubernetes#install-helm-and-the-secrets-store-csi-driver>

`helm repo add csi-secrets-store-provider-azure https://raw.githubusercontent.com/Azure/secrets-store-csi-driver-provider-azure/master/charts`
cd to your prefered folder and download the chart:
`helm pull https://github.com/Azure/secrets-store-csi-driver-provider-azure . --untar`
install the chart:
`helm install csi-driver  ./csi-secrets-store-provider-azure`
check that you have pods and daemonsets installed
