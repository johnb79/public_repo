# Introduction 
Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. 
Terraform can manage existing and popular service providers as well as custom in-house solutions.
Configuration files describe to Terraform the components needed to run a single application or an entire datacenter.

# Automation targets
We want to reuse the same code basis to create identical or different environments, fully automated, with variable values assigned automatically
by a CI/CD tool like Azure DevOps. 
For example we need three enviroments Dev,Tst and Prod. These environments have a webapp, a sql server and an application gateway.
In this case, a single env_master configured with terraform modules can serve Dev, Tst and Prod by simply assigning different variable values per environment
from your CI/CD tool such as Azure DevOps.
Assuming you dont want to have an application gateway in your Dev environment, you can create an env_dev folder, configure only the webapp and sql server modules
and then your env_master folder configuring a webapp, a sql server and an application gateway. Again values for the variables per environment are provided by
the CI/CD tool without any confilcts per environment.

# Terraform Architecture
Terraform is designed with modules architecture. 
Modules folder is organized as described in terraform's documentation. For example, inside modules/databace_rcs folder you will find all the modules
related to Terraform's documentation --> Database Resources. As a result, all existing and future modules related to databases should live inside 
databace_rcs folder.      https://www.terraform.io/docs/providers/azurerm/r/sql_database.html

We also use TF_VAR_ variables to set values using enviroment variables. Adding TF_VAR_name environment variables in our CI/CD tool,
Terraform will search for environment variables. 
Inside env_sample folder you can find working examples of how to configure each resource. The env_sample folder is organized per resource group which means
every file which starts with rg_ reperesents the resources configured inside an Azure resource group.
The terr_init file configures the azurerm provider, terraform's backend storage for .tfstate and also the tags shared between resources.

# Getting started
1) Install Terraform in your laptop
2) Create a folder
3) cd /folder_name
4) git clone 
5) Navigate inside env_sample
6) <Login to your Azure subscription and create the storage account for the azure backend to store remote .tfstate>

     az group create -n terraform-tfstate -l uksouth 
     az storage account create -n terrstatedev -g terraform-tfstate --location uksouth
     az storage container create --name terraformdev --account-name terrstatedev

7) Open env_sample in your editor. 
8) Go to terr_init.tf and configure azure backend. <If you used the az commands as provided no changes needed to the : backend "azurerm">. 
  terr_init azure_backend_config is only needed when testing the code outside CI/CD tool. Remove or comment out the azure backend config when set up is through
  Azure DevOps. The backend is configured at the pipeline level with the Terraform tasks.
9) From your terminal: terraform init
10) If terraform returns an error, login to your Azure subscription and verify the storage account you configured earlier in terr_init.tf
11) Terraform plan <if code inside rg_<name>.tf is commended # , remove the comments # >

# Config Example

#  Resource Group Variables 
variable "KEY_VAULT_ENV_PREFIX" {}   *<-- TF_VAR value will be picked up from the environment variables*
variable "KEY_VAULT_RESOURCE_GROUP_NAME" { default = "key-vault" }   *<-- to test from your terminal, add a value as shown.*
variable "KEY_VAULT_RESOURCE_GROUP_LOCATION" { default = "uksouth" }



##################################################################################################################################

# Create Resource Group
module "key-vault_01_resource_group" {
  source = "../modules/base_rc/az_resource_group"

  resource_group_name     = "${var.KEY_VAULT_ENV_PREFIX}-${var.KEY_VAULT_RESOURCE_GROUP_NAME}"
  resource_group_location = var.KEY_VAULT_RESOURCE_GROUP_LOCATION
  environment_tags        = var.default_tags
}
