#  Resource Group Variables 
variable "WEBAPP_ENV_PREFIX" { default = "task-list-app" }
variable "WEBAPP_RESOURCE_GROUP_NAME" { default = "webapp" }
variable "WEBAPP_RESOURCE_GROUP_LOCATION" { default = "uksouth" }


#  Web App Variables 
variable "WEBAPP_APP_SERVICE_PLAN_NAME" { default = "tl-cont-web-app-srv-plan" }
variable "WEBAPP_APP_SERVICE_NAME" { default = "tl-cont-web-app-srv" }

#  Web App Slot Variables 
variable "APP_SERVICE_SLOT_NAME" { default = "staging" }


##############################################################################################################################

# Create Resource Group
module "web_app_01_resource_group" {
  source = "../modules/base_rcs/az_resource_group"

  resource_group_name     = "${var.WEBAPP_ENV_PREFIX}-${var.WEBAPP_RESOURCE_GROUP_NAME}"
  resource_group_location = var.WEBAPP_RESOURCE_GROUP_LOCATION
  environment_tags        = var.default_tags
}

# Get the Azure Container Registry
data "azurerm_container_registry" "containers_web_app_01" {
  name                = "contimageregistry"
  resource_group_name = "container-registry-app"
}

# Get the Azure Keyvalt Values
data "azurerm_key_vault" "containers_web_app_01" {
  name                = "terraform-keyvault01"
  resource_group_name = "azure-key-vault"
}
data "azurerm_key_vault_secret" "containers_web_app_01" {
  name         = "sql-connection-string"
  key_vault_id = data.azurerm_key_vault.containers_web_app_01.id
}

# Create Linux Web App for Containers
module "web_app_containers_01" {
  source = "../modules/app_service_(web_apps)_rcs/az_webapp_containers"

  # App Service Plan   
  app_service_plan_name                = var.WEBAPP_APP_SERVICE_PLAN_NAME
  app_service_plan_location            = "${module.web_app_01_resource_group.resource_group_location}"
  app_service_plan_resource_group_name = "${module.web_app_01_resource_group.resource_group_name}"
  environment_tags                     = var.default_tags

  # App Service  
  app_service_name                = var.WEBAPP_APP_SERVICE_NAME
  app_service_location            = "${module.web_app_01_resource_group.resource_group_location}"
  app_service_resource_group_name = "${module.web_app_01_resource_group.resource_group_name}"

  # Web App Settings
  app_settings = {
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE"               = false
    "DOCKER_REGISTRY_SERVER_URL"                        = "https://${data.azurerm_container_registry.containers_web_app_01.login_server}"
    "APP_SETTINGS_SLOT_DOCKER_REGISTRY_SERVER_USERNAME" = data.azurerm_container_registry.containers_web_app_01.admin_username
    "DOCKER_REGISTRY_SERVER_PASSWORD"                   = data.azurerm_container_registry.containers_web_app_01.admin_password
    "ASPNETCORE_ENVIRONMENT"                            = "Production"

  }

  # Web app Connection String
  app_service_connection_string_name  = "MyDbConnection"
  app_service_connection_string_type  = "SQLAzure"
  app_service_connection_string_value = data.azurerm_key_vault_secret.containers_web_app_01.value

}
# Create Linux Web App Slot for Containers
module "web_app_containers_01_slot" {
  source = "../modules/app_service_(web_apps)_rcs/az_webapp_containers_slot"

  # App Service Slot   
  app_service_slot_name                = var.APP_SERVICE_SLOT_NAME
  app_service_slot_app_service_name    = "${module.web_app_containers_01.app_service_name}"
  app_service_slot_location            = "${module.web_app_containers_01.app_service_location}"
  app_service_slot_resource_group_name = "${module.web_app_containers_01.app_service_resource_group_name}"
  app_service_slot_app_service_plan_id = "${module.web_app_containers_01.app_service_plan_id}"
  environment_tags                     = var.default_tags

  # Web App Slot Settings
  app_slot_settings = {
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE"               = false
    "DOCKER_REGISTRY_SERVER_URL"                        = "https://${data.azurerm_container_registry.containers_web_app_01.login_server}"
    "APP_SETTINGS_SLOT_DOCKER_REGISTRY_SERVER_USERNAME" = data.azurerm_container_registry.containers_web_app_01.admin_username
    "DOCKER_REGISTRY_SERVER_PASSWORD"                   = data.azurerm_container_registry.containers_web_app_01.admin_password
    "ASPNETCORE_ENVIRONMENT"                            = "Production"

  }
  # Web app Connection String
  app_service_slot_connection_string_name  = "MyDbConnection"
  app_service_slot_connection_string_type  = "SQLAzure"
  app_service_slot_connection_string_value = data.azurerm_key_vault_secret.containers_web_app_01.value


}
