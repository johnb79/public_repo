provider "azurerm" {
  # Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "~> 1.39.0"

}
terraform {
  backend "azurerm" {}
}

# terraform {
#   backend "azurerm" {
#     resource_group_name  = "terraform-tfstate"
#     storage_account_name = "terrstatedev"
#     container_name       = "terraformdev"
#     key                  = "terr_task_list_containers.tfstate"
#   }
# }

variable "default_tags" {
  type = map
  default = {
    Owner : "JohnB"
    Cost_Center : "12345678"
    Customer : "i_dont_pay_a_penny"
    Project : "Main_Website"
    Data_Class : "Public"
    Compliance : "N/A"
  }
}
