#  Resource Group Variables 
variable "VNET_ENV_PREFIX" { default = "task-list-app" }
variable "VNET_RESOURCE_GROUP_NAME" { default = "vnet" }
variable "VNET_RESOURCE_GROUP_LOCATION" { default = "uksouth" }

# Virtual Network Variables
variable "VIRTUAL_NETWORK_NAME" { default = "vnet-01" }

# Virtual Network Subnet Variables
variable "VIRTUAL_NETWORK_SUBNET_NAME" { default = "vnet-subnet-01" }

# Public IP Variables
variable "PUBLIC_IP_NAME" { default = "public-ip-01" }

# Application Gateway Variables
variable "APPLICATION_GATEWAY_NAME" { default = "test-app-appgtw" }

##############################################################################################################################

# Create Resource Group
module "virtual_network_01_resource_group" {
  source = "../modules/base_rcs/az_resource_group"

  resource_group_name     = "${var.VNET_ENV_PREFIX}-${var.VNET_RESOURCE_GROUP_NAME}"
  resource_group_location = var.VNET_RESOURCE_GROUP_LOCATION
  environment_tags        = var.default_tags
}

# Create Virtual Network
module "virtual_network_01" {
  source = "../modules/network_rcs/az_virtual_network"

  virtual_network_name                = "${var.VNET_ENV_PREFIX}-${var.VIRTUAL_NETWORK_NAME}"
  virtual_network_location            = module.virtual_network_01_resource_group.resource_group_location
  virtual_network_resource_group_name = module.virtual_network_01_resource_group.resource_group_name
  virtual_network_address_space       = ["10.0.0.0/16"]
  virtual_network_dns_servers         = ["10.0.0.4", "10.0.0.5"]
  environment_tags                    = var.default_tags
}

# Create Virtual Network Subnet
module "virtual_network_subnet_01" {
  source = "../modules/network_rcs/az_virtual_network_subnet"

  virtual_network_subnet_name                                  = "${var.VNET_ENV_PREFIX}-${var.VIRTUAL_NETWORK_SUBNET_NAME}"
  virtual_network_subnet_resource_group_name                   = module.virtual_network_01_resource_group.resource_group_name
  virtual_network_subnet_virtual_network_name                  = module.virtual_network_01.virtual_network_name
  virtual_network_subnet_address_prefix                        = "10.0.1.0/24"
  virtual_network_subnet_network_security_group_association_id = null

}

# Create Public IP
module "virtual_network_01_public_ip" {
  source = "../modules/network_rcs/az_public_ip"

  public_ip_name                = "${var.VNET_ENV_PREFIX}-${var.PUBLIC_IP_NAME}"
  public_ip_resource_group_name = module.virtual_network_01_resource_group.resource_group_name
  public_ip_location            = module.virtual_network_01_resource_group.resource_group_location
  public_ip_domain_name_label   = null
  environment_tags              = var.default_tags
}

# Create Application Gateway  // Nested blocks inside allow for mutiple apps config, see terraform dynamic blocks
module "virtual_network_01_application_gateway" {
  source = "../modules/network_rcs/az_application_gateway"

  application_gateway_name                = var.APPLICATION_GATEWAY_NAME
  application_gateway_resource_group_name = module.virtual_network_01_resource_group.resource_group_name
  application_gateway_location            = module.virtual_network_01_resource_group.resource_group_location
  environment_tags                        = var.default_tags

  # Application Gateway sku
  application_gateway_sku_name     = "WAF_v2"
  application_gateway_sku_tier     = "WAF_v2"
  application_gateway_sku_capacity = 2

  # Application Gateway gateway_ip_configuration
  application_gateway_ip_configuration_name      = module.virtual_network_01_public_ip.public_ip_name
  application_gateway_ip_configuration_subnet_id = module.virtual_network_subnet_01.subnet_id

  # Application Gateway frontend_ip_configuration
  application_gateway_frontend_ip_configuration_name                 = module.virtual_network_01_public_ip.public_ip_name
  application_gateway_frontend_ip_configuration_public_ip_address_id = module.virtual_network_01_public_ip.public_ip_id

  # Application Gateway frontend_port
  application_gateway_frontend_port_name = "test"
  application_gateway_frontend_port_port = 80

  # Application Gateway backend_address_pool // configure more than one backend pools
  # Optional variables are not possible on dynamic blocks yet, for fqdns add them to the module first and then set them.
  backend_address_pool = [
    {
      name = "backAddressPool"

    },
    {
      name = "secondBackAddressPool"

    }
  ]

  # Application Gateway backend_http_settings  // configure more than one backend_http_setting
  backend_http_settings = [
    {
      name                                = "backendHttpSettings"
      cookie_based_affinity               = "Disabled"
      path                                = "/"
      port                                = 80
      protocol                            = "http"
      request_timeout                     = 30
      pick_host_name_from_backend_address = true
    }
  ]
  # Application Gateway http_listener // configure more than one http_listener
  http_listener = [
    {
      name                           = "httpListener"
      frontend_ip_configuration_name = module.virtual_network_01_public_ip.public_ip_name
      frontend_port_name             = "test"
      protocol                       = "http"

    }
  ]

  # Application Gateway probe  // configure more than one probe
  probe = [
    {
      name                                      = "testprobe"
      protocol                                  = "http"
      path                                      = "/"
      interval                                  = 30
      timeout                                   = 60
      unhealthy_threshold                       = 10
      pick_host_name_from_backend_http_settings = true
    }
  ]
  # Application Gateway request_routing_rule // configure more than one request_routing_rule
  request_routing_rule = [
    {
      name                       = "routingrule"
      rule_type                  = "Basic"
      http_listener_name         = "httpListener"
      backend_address_pool_name  = "backAddressPool"
      backend_http_settings_name = "backendHttpSettings"
    }
  ]


  application_gateway_waf_configuration_enabled          = true
  application_gateway_waf_configuration_firewall_mode    = "Prevention"
  application_gateway_waf_configuration_rule_set_type    = "OWASP"
  application_gateway_waf_configuration_rule_set_version = "3.0"

}
