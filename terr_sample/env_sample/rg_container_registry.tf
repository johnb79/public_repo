# #  Resource Group 
# variable "ACR_ENV_PREFIX" { default = "task-list-app" }
# variable "ACR_RESOURCE_GROUP_NAME" { default = "container-registry" }
# variable "ACR_RESOURCE_GROUP_LOCATION" { default = "uksouth" }

# # Container Registry
# variable "ACR_CONTAINER_REGISTRY_NAME" { default = "TaskListAppRegistry" }
# variable "ACR_CONTAINER_REGISTRY_SKU" { default = "Standard" }

# #############################################################################################################################


# # Create Resource Group
# module "acr_resource_group" {
#   source = "../modules/base_rcs/az_resource_group"

#   resource_group_name     = "${var.ACR_ENV_PREFIX}-${var.ACR_RESOURCE_GROUP_NAME}"
#   resource_group_location = var.ACR_RESOURCE_GROUP_LOCATION
#   environment_tags        = var.default_tags
# }
# # Create Container Registry
# module "acr_container_registry" {
#   source                      = "../modules/container_rcs/az_container_registry"
#   container_registry_name     = var.ACR_CONTAINER_REGISTRY_NAME
#   container_registry_sku      = var.ACR_CONTAINER_REGISTRY_SKU
#   container_registry_rg_name  = "${module.acr_resource_group.resource_group_name}"
#   container_registry_location = "${module.acr_resource_group.resource_group_location}"
#   environment_tags            = var.default_tags
# }
