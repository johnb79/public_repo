# #  Resource Group Variables 
# variable "FUNCTION_APP_ENV_PREFIX" { default = "task-list-app" }
# variable "FUNCTION_APP_RESOURCE_GROUP_NAME" { default = "function" }
# variable "FUNCTION_APP_RESOURCE_GROUP_LOCATION" { default = "uksouth" }





# ##############################################################################################################################

# # Create Resource Group
# module "function_app_01_resource_group" {
#   source = "../modules/base_rcs/az_resource_group"

#   resource_group_name     = "${var.FUNCTION_APP_ENV_PREFIX}-${var.FUNCTION_APP_RESOURCE_GROUP_NAME}"
#   resource_group_location = var.FUNCTION_APP_RESOURCE_GROUP_LOCATION
#   environment_tags        = var.default_tags
# }

# # Create function storage account
# resource "azurerm_storage_account" "function_app_01" {
#   name                     = "johnbfunction01"
#   resource_group_name      = module.function_app_01_resource_group.resource_group_name
#   location                 = module.function_app_01_resource_group.resource_group_location
#   account_tier             = "Standard"
#   account_replication_type = "LRS"
# }

# # Create function service plan
# resource "azurerm_app_service_plan" "function_app_01" {
#   name                = "azure-functions-johnb-service-plan"
#   location            = module.function_app_01_resource_group.resource_group_location
#   resource_group_name = module.function_app_01_resource_group.resource_group_name

#   sku {
#     tier = "Standard"
#     size = "S1"
#   }
# }

# # Create function 
# resource "azurerm_function_app" "function_app_01" {
#   name                      = "johnb-azure-function"
#   location                  = module.function_app_01_resource_group.resource_group_location
#   resource_group_name       = module.function_app_01_resource_group.resource_group_name
#   app_service_plan_id       = azurerm_app_service_plan.function_app_01.id
#   storage_connection_string = azurerm_storage_account.function_app_01.primary_connection_string
# }

# # Create function slot
# resource "azurerm_app_service_slot" "example" {
#   name                = "staging"
#   app_service_name    = azurerm_function_app.function_app_01.name
#   location            = azurerm_function_app.function_app_01.location
#   resource_group_name = module.function_app_01_resource_group.resource_group_name
#   app_service_plan_id = azurerm_app_service_plan.function_app_01.id

# }
