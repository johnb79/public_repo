# #  Resource Group Variables 
# variable "KEY_VAULT_ENV_PREFIX" { default = "task-list-app" }
# variable "KEY_VAULT_RESOURCE_GROUP_NAME" { default = "key-vault" }
# variable "KEY_VAULT_RESOURCE_GROUP_LOCATION" { default = "uksouth" }

# # Key Vault Variables
# variable "KEY_VAULT_NAME" { default = "kv-01" }

# ##############################################################################################################################

# # Create Resource Group
# module "key_vault_01_resource_group" {
#   source = "../modules/base_rcs/az_resource_group"

#   resource_group_name     = "${var.KEY_VAULT_ENV_PREFIX}-${var.KEY_VAULT_RESOURCE_GROUP_NAME}"
#   resource_group_location = var.KEY_VAULT_RESOURCE_GROUP_LOCATION
#   environment_tags        = var.default_tags
# }

# # Get Azure Tenant Credentials
# data "azurerm_client_config" "current" {}


# # Create Key Vault
# module "key_vault_01" {
#   source = "../modules/key_vault_rcs/az_key_vault"

#   key_vault_name                              = "${var.KEY_VAULT_ENV_PREFIX}-${var.KEY_VAULT_NAME}"
#   key_vault_location                          = module.key_vault_01_resource_group.resource_group_location
#   key_vault_resource_group_name               = module.key_vault_01_resource_group.resource_group_name
#   key_vault_tenant_id                         = data.azurerm_client_config.current.tenant_id
#   key_vault_access_policy_tenant_id           = data.azurerm_client_config.current.tenant_id
#   key_vault_access_policy_object_id           = data.azurerm_client_config.current.object_id
#   key_vault_access_policy_key_permissions     = ["get", "create", "list"]
#   key_vault_access_policy_secret_permissions  = ["get", "list", "set", "restore"]
#   key_vault_access_policy_storage_permissions = null
#   environment_tags                            = var.default_tags
# }
