#  Resource Group Variables 
variable "SQL_SERVER_ENV_PREFIX" { default = "task-list-app" }
variable "SQL_SERVER_RESOURCE_GROUP_NAME" { default = "sql-server" }
variable "SQL_SERVER_RESOURCE_GROUP_LOCATION" { default = "uksouth" }

# Sql Server Variables
variable "SQL_SERVER_NAME" { default = "sql-server-01" }
variable "SQL_SERVER_ADMIN_LOGIN" { default = "mradministrator" }
variable "SQL_SERVER_ADMIN_PASSWORD" { default = "thisIsDog11" }

# Sql Server Database Variables
variable "SQL_DATABASE_NAME" { default = "sql-database-01" }

##############################################################################################################################

# Create Resource Group
module "sql_server_01_resource_group" {
  source = "../modules/base_rcs/az_resource_group"

  resource_group_name     = "${var.SQL_SERVER_ENV_PREFIX}-${var.SQL_SERVER_RESOURCE_GROUP_NAME}"
  resource_group_location = var.SQL_SERVER_RESOURCE_GROUP_LOCATION
  environment_tags        = var.default_tags
}

# Create SQL Server
module "sql_server_01" {
  source = "../modules/database_rcs/az_sql_server"

  sql_server_name                = "${var.SQL_SERVER_ENV_PREFIX}-${var.SQL_SERVER_NAME}"
  sql_server_resource_group_name = module.sql_server_01_resource_group.resource_group_name
  sql_server_location            = module.sql_server_01_resource_group.resource_group_location
  sql_server_admin_login         = var.SQL_SERVER_ADMIN_LOGIN
  sql_server_admin_password      = var.SQL_SERVER_ADMIN_PASSWORD
  environment_tags               = var.default_tags
}

# Create SQL Server Database
module "sql_server_database_01" {
  source = "../modules/database_rcs/az_sql_database"

  sql_database_name                = var.SQL_DATABASE_NAME
  sql_database_resource_group_name = module.sql_server_01_resource_group.resource_group_name
  sql_database_location            = module.sql_server_01_resource_group.resource_group_location
  sql_database_server_name         = module.sql_server_01.sql_server_name
  environment_tags                 = var.default_tags
}
