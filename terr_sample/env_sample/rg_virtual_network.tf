# #  Resource Group Variables 
# variable "VNET_ENV_PREFIX" { default = "task-list-app" }
# variable "VNET_RESOURCE_GROUP_NAME" { default = "vnet" }
# variable "VNET_RESOURCE_GROUP_LOCATION" { default = "uksouth" }

# # Virtual Network Variables
# variable "VIRTUAL_NETWORK_NAME" { default = "vnet-01" }

# # Virtual Network Subnet Variables
# variable "VIRTUAL_NETWORK_SUBNET_NAME" { default = "vnet-subnet-01" }

# # Virtual Network Security Group Variables
# variable "NETWORK_SECURITY_GROUP_NAME" { default = "vnet-subnet-nsg-01" }

# # Public IP Variables
# variable "PUBLIC_IP_NAME" { default = "public-ip-01" }

# ##############################################################################################################################

# # Create Resource Group
# module "virtual_network_01_resource_group" {
#   source = "../modules/base_rcs/az_resource_group"

#   resource_group_name     = "${var.VNET_ENV_PREFIX}-${var.VNET_RESOURCE_GROUP_NAME}"
#   resource_group_location = var.VNET_RESOURCE_GROUP_LOCATION
#   environment_tags        = var.default_tags
# }

# # Create Virtual Network
# module "virtual_network_01" {
#   source = "../modules/network_rcs/az_virtual_network"

#   virtual_network_name                = "${var.VNET_ENV_PREFIX}-${var.VIRTUAL_NETWORK_NAME}"
#   virtual_network_location            = module.virtual_network_01_resource_group.resource_group_location
#   virtual_network_resource_group_name = module.virtual_network_01_resource_group.resource_group_name
#   virtual_network_address_space       = ["10.0.0.0/16"]
#   virtual_network_dns_servers         = ["10.0.0.4", "10.0.0.5"]
#   environment_tags                    = var.default_tags
# }

# # Create Virtual Network Subnet
# module "virtual_network_subnet_01" {
#   source = "../modules/network_rcs/az_virtual_network_subnet"

#   virtual_network_subnet_name                                  = "${var.VNET_ENV_PREFIX}-${var.VIRTUAL_NETWORK_SUBNET_NAME}"
#   virtual_network_subnet_resource_group_name                   = module.virtual_network_01_resource_group.resource_group_name
#   virtual_network_subnet_virtual_network_name                  = module.virtual_network_01.virtual_network_name
#   virtual_network_subnet_address_prefix                        = "10.0.1.0/24"
#   virtual_network_subnet_network_security_group_association_id = module.virtual_network_security_group_01.security_group_id

# }

# # Create Virtual Network Network Security Group
# module "virtual_network_security_group_01" {
#   source = "../modules/network_rcs/az_network_security_group"

#   network_security_group_name                      = "${var.VNET_ENV_PREFIX}-${var.NETWORK_SECURITY_GROUP_NAME}"
#   network_security_group_location                  = module.virtual_network_01_resource_group.resource_group_location
#   network_security_group_resource_group_name       = module.virtual_network_01_resource_group.resource_group_name
#   network_security_rule_resource_group_name        = module.virtual_network_01_resource_group.resource_group_name
#   virtual_network_subnet_security_group_subnet_id  = module.virtual_network_subnet_01.subnet_id
#   virtual_network_subnet_network_security_group_id = module.virtual_network_security_group_01.security_group_id


#   nsg_rules_01 = {
#     "0" = "Inbound,101,nsg-sec-rule,*,TCP,*,443,Allow"
#     "1" = "Inbound,102,nsg-sec-range-rule,*,TCP,*,65200-65535,Allow"
#     "2" = "Inbound,103,nsg-sec-80-rule,*,TCP,*,80,Allow"
#     "3" = "Outbound,101,nsg-sec-22-rule,*,TCP,*,22,Deny"
#   }
# }

# # Create Public IP
# module "virtual_network_01_public_ip" {
#   source = "../modules/network_rcs/az_public_ip"

#   public_ip_name                = "${var.VNET_ENV_PREFIX}-${var.PUBLIC_IP_NAME}"
#   public_ip_resource_group_name = module.virtual_network_01_resource_group.resource_group_name
#   public_ip_location            = module.virtual_network_01_resource_group.resource_group_location
#   public_ip_domain_name_label   = null
#   environment_tags              = var.default_tags
# }
