# #  Resource Group Variables 
# variable "WEBAPP_ENV_PREFIX" { default = "task-list-app" }
# variable "WEBAPP_RESOURCE_GROUP_NAME" { default = "webapp" }
# variable "WEBAPP_RESOURCE_GROUP_LOCATION" { default = "uksouth" }


# #  Web App Variables 
# variable "WEBAPP_APP_SERVICE_PLAN_NAME" { default = "tl-cont-web-app-srv-plan" }
# variable "WEBAPP_APP_SERVICE_NAME" { default = "tl-cont-web-app-srv" }

# #  Web App Slot Variables 
# variable "APP_SERVICE_SLOT_NAME" { default = "staging" }


# # Web App Settings Variables
# variable "WEBSITES_ENABLE_APP_SERVICE_STORAGE" { default = true }

# ##############################################################################################################################

# # Create Resource Group
# module "web_app_01_resource_group" {
#   source = "../modules/base_rcs/az_resource_group"

#   resource_group_name     = "${var.WEBAPP_ENV_PREFIX}-${var.WEBAPP_RESOURCE_GROUP_NAME}"
#   resource_group_location = var.WEBAPP_RESOURCE_GROUP_LOCATION
#   environment_tags        = var.default_tags
# }

# # Get the Azure Container Registry
# data "azurerm_container_registry" "containers_web_app_01" {
#   name                = module.acr_container_registry.acr_01_name
#   resource_group_name = module.acr_resource_group.resource_group_name

# }

# # Create Linux Web App for Containers
# module "web_app_containers_01" {
#   source = "../modules/app_service_(web_apps)_rcs/az_webapp_containers"

#   # App Service Plan   
#   app_service_plan_name                = var.WEBAPP_APP_SERVICE_PLAN_NAME
#   app_service_plan_location            = "${module.web_app_01_resource_group.resource_group_location}"
#   app_service_plan_resource_group_name = "${module.web_app_01_resource_group.resource_group_name}"
#   environment_tags                     = var.default_tags

#   # App Service  
#   app_service_name                = var.WEBAPP_APP_SERVICE_NAME
#   app_service_location            = "${module.web_app_01_resource_group.resource_group_location}"
#   app_service_resource_group_name = "${module.web_app_01_resource_group.resource_group_name}"

#   # Web App Settings
#   app_settings_websites_enable_app_service_storage = var.WEBSITES_ENABLE_APP_SERVICE_STORAGE
#   app_settings_docker_registry_server_url          = "https://${data.azurerm_container_registry.containers_web_app_01.login_server}"
#   app_settings_docker_registry_server_username     = data.azurerm_container_registry.containers_web_app_01.admin_username
#   app_settings_docker_registry_server_password     = data.azurerm_container_registry.containers_web_app_01.admin_password
# }

# # Create Linux Web App Slot for Containers
# module "web_app_containers_01_slot" {
#   source = "../modules/app_service_(web_apps)_rcs/az_webapp_containers_slot"

#   # App Service Slot   
#   app_service_slot_name                = var.APP_SERVICE_SLOT_NAME
#   app_service_slot_app_service_name    = "${module.web_app_containers_01.app_service_name}"
#   app_service_slot_location            = "${module.web_app_containers_01.app_service_location}"
#   app_service_slot_resource_group_name = "${module.web_app_containers_01.app_service_resource_group_name}"
#   app_service_slot_app_service_plan_id = "${module.web_app_containers_01.app_service_plan_id}"
#   environment_tags                     = var.default_tags


#   # Web App Settings
#   app_settings_slot_websites_enable_app_service_storage = var.WEBSITES_ENABLE_APP_SERVICE_STORAGE
#   app_settings_slot_docker_registry_server_url          = "https://${data.azurerm_container_registry.containers_web_app_01.login_server}"
#   app_settings_slot_docker_registry_server_username     = data.azurerm_container_registry.containers_web_app_01.admin_username
#   app_settings_slot_docker_registry_server_password     = data.azurerm_container_registry.containers_web_app_01.admin_password
# }
