#  Resource Group Variables 
variable "WEB_APP_ENV_PREFIX" {}
variable "WEB_APP_RESOURCE_GROUP_NAME" {}
variable "WEB_APP_RESOURCE_GROUP_LOCATION" {}


#  Web App Variables 
variable "WEB_APP_SERVICE_PLAN_NAME" {}
variable "WEB_APP_SERVICE_NAME" {}

#  Web App Slot Variables 
variable "WEB_APP_SERVICE_SLOT_NAME" {}


##############################################################################################################################

# Create Resource Group
module "web_app_01_resource_group" {
  source = "../modules/base_rcs/az_resource_group"

  resource_group_name     = "${var.WEB_APP_ENV_PREFIX}-${var.WEB_APP_RESOURCE_GROUP_NAME}"
  resource_group_location = var.WEB_APP_RESOURCE_GROUP_LOCATION
  environment_tags        = var.default_tags
}


# Create Linux Web App 
module "web_app_01" {
  source = "../modules/app_service_(web_apps)_rcs/az_webapp"

  # App Service Plan   
  web_app_service_plan_name                = var.WEB_APP_SERVICE_PLAN_NAME
  web_app_service_plan_location            = module.web_app_01_resource_group.resource_group_location
  web_app_service_plan_resource_group_name = module.web_app_01_resource_group.resource_group_name
  web_app_service_plan_sku_tier            = "Standard"
  web_app_service_plan_sku_size            = "S1"

  environment_tags = var.default_tags

  # App Service  
  web_app_service_name                  = var.WEB_APP_SERVICE_NAME
  web_app_service_location              = module.web_app_01_resource_group.resource_group_location
  web_app_service_resource_group_name   = module.web_app_01_resource_group.resource_group_name
  web_app_service_application_framework = "NODE|10.14"

  # Web App Settings
  web_app_settings = {
    "CH_KEY"      = "test"
    "ENV"         = "linux"
    "NODE_ENV"    = "10"
    "SERVER_HOST" = "test_server"

  }

  # Web App Connection String
  # web_app_connection_string_name  = ""
  # web_app_connection_string_type  = ""
  # web_app_connection_string_value = "" 

}

# Create Linux Web App Slot for Containers
module "web_app_01_slot" {
  source = "../modules/app_service_(web_apps)_rcs/az_webapp_slot"

  # App Service Slot   
  web_app_service_slot_name                = var.WEB_APP_SERVICE_SLOT_NAME
  web_app_service_slot_app_service_name    = module.web_app_01.web_app_service_name
  web_app_service_slot_location            = module.web_app_01.web_app_service_location
  web_app_service_slot_resource_group_name = module.web_app_01.web_app_service_resource_group_name
  web_app_service_slot_app_service_plan_id = module.web_app_01.web_app_service_plan_id
  environment_tags                         = var.default_tags


  # Web App Slot Settings
  web_app_slot_settings = {
    "CH_KEY"      = "test"
    "ENV"         = "linux"
    "NODE_ENV"    = "10"
    "SERVER_HOST" = "test_server"
    "SERVER_NAME" = "server_name"

  }

  # Web App Connection String
  # web_app_connection_string_name  = ""
  # web_app_connection_string_type  = ""
  # web_app_connection_string_value = ""



}
