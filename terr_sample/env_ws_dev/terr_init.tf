provider "azurerm" {
  # Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "~> 1.39.0"

}
terraform {
  backend "azurerm" {}
}

variable "default_tags" {
  type = map
  default = {
    Owner : "johnb"
    Cost_Center : "123456"
    Customer : "myself-ltd"
    Project : "Main_Website"
    Data_Class : "Public"
    Compliance : "N/A"
  }
}
