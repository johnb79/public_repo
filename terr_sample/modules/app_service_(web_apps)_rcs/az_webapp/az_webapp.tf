

resource "azurerm_app_service_plan" "web_app_01" {
  name                = var.web_app_service_plan_name
  location            = var.web_app_service_plan_location
  resource_group_name = var.web_app_service_plan_resource_group_name
  kind                = "Linux"
  reserved            = true

  sku {
    tier = var.web_app_service_plan_sku_tier
    size = var.web_app_service_plan_sku_size
  }
  tags = var.environment_tags
}


resource "azurerm_app_service" "web_app_01" {
  name                = var.web_app_service_name
  location            = var.web_app_service_location
  resource_group_name = var.web_app_service_resource_group_name
  app_service_plan_id = azurerm_app_service_plan.web_app_01.id

  site_config {
    linux_fx_version = var.web_app_service_application_framework
  }
  app_settings = var.web_app_settings


  #   connection_string {
  #   name  = var.web_app_connection_string_name
  #   type  = var.web_app_connection_string_type
  #   value = var.web_app_connection_string_value
  # }

  tags = var.environment_tags

}
