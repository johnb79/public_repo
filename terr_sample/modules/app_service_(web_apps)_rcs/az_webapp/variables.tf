variable "web_app_service_plan_name" {}
variable "web_app_service_plan_location" {}
variable "web_app_service_plan_resource_group_name" {}
variable "web_app_service_plan_sku_tier" {}
variable "web_app_service_plan_sku_size" {}

variable "web_app_service_name" {}
variable "web_app_service_location" {}
variable "web_app_service_resource_group_name" {}
variable "web_app_service_application_framework" {}


variable "web_app_settings" { type = map }

# variable "web_app_connection_string_name" {}
# variable "web_app_connection_string_type" {}
# variable "web_app_connection_string_value" {}

variable "environment_tags" {}
