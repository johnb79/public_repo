
# Multi-container with Docker Compose  (enable admin access in ACR settings)
# https://docs.microsoft.com/en-us/azure/app-service/containers/app-service-linux-faq#multi-container-with-docker-compose
resource "azurerm_app_service_plan" "containers_web_app_01" {
  name                = var.app_service_plan_name
  location            = var.app_service_plan_location
  resource_group_name = var.app_service_plan_resource_group_name
  kind                = "Linux"
  reserved            = true

  sku {
    tier = "Standard"
    size = "S1"
  }
  tags = var.environment_tags
}


resource "azurerm_app_service" "containers_web_app_01" {
  name                = var.app_service_name
  location            = var.app_service_location
  resource_group_name = var.app_service_resource_group_name
  app_service_plan_id = azurerm_app_service_plan.containers_web_app_01.id

  site_config {
    linux_fx_version = "COMPOSE|${filebase64("${path.module}/compose2.yml")}"
    scm_type         = "VSTSRM"
  }

  lifecycle {
    ignore_changes = [
      site_config[0].linux_fx_version
    ]
  }
  connection_string {
    name  = var.app_service_connection_string_name
    type  = var.app_service_connection_string_type
    value = var.app_service_connection_string_value
  }

  app_settings = var.app_settings

  tags = var.environment_tags

}
