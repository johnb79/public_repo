output "app_service_name" {
  value = azurerm_app_service.containers_web_app_01.name
}
output "app_service_location" {
  value = azurerm_app_service.containers_web_app_01.location
}
output "app_service_resource_group_name" {
  value = azurerm_app_service.containers_web_app_01.resource_group_name
}
output "app_service_plan_id" {
  value = azurerm_app_service_plan.containers_web_app_01.id
}