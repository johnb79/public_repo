variable "app_service_plan_name" {}
variable "app_service_plan_location" {}
variable "app_service_plan_resource_group_name" {}

variable "app_service_name" {}
variable "app_service_location" {}
variable "app_service_resource_group_name" {}

variable "app_settings" { type = map }

variable "app_service_connection_string_name" {}
variable "app_service_connection_string_type" {}
variable "app_service_connection_string_value" {}

variable "environment_tags" {}
