resource "azurerm_app_service_slot" "containers_web_app_01" {
  name                = var.app_service_slot_name
  app_service_name    = var.app_service_slot_app_service_name
  location            = var.app_service_slot_location
  resource_group_name = var.app_service_slot_resource_group_name
  app_service_plan_id = var.app_service_slot_app_service_plan_id
  tags                = var.environment_tags
  site_config {
    linux_fx_version = "COMPOSE|${filebase64("${path.module}/compose2.yml")}"
    scm_type         = "VSTSRM"
  }

  lifecycle {
    ignore_changes = [
      site_config[0].linux_fx_version
    ]
  }

  connection_string {
    name  = var.app_service_slot_connection_string_name
    type  = var.app_service_slot_connection_string_type
    value = var.app_service_slot_connection_string_value
  }

  app_settings = var.app_slot_settings
}
