variable "app_service_slot_name" {} # staging
variable "app_service_slot_app_service_name" {}
variable "app_service_slot_location" {}
variable "app_service_slot_resource_group_name" {}
variable "app_service_slot_app_service_plan_id" {}
variable "environment_tags" {}

variable "app_service_slot_connection_string_name" {}
variable "app_service_slot_connection_string_type" {}
variable "app_service_slot_connection_string_value" {}

variable "app_slot_settings" { type = map }
