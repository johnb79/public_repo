resource "azurerm_app_service_slot" "web_app_01" {
  name                = var.web_app_service_slot_name
  app_service_name    = var.web_app_service_slot_app_service_name
  location            = var.web_app_service_slot_location
  resource_group_name = var.web_app_service_slot_resource_group_name
  app_service_plan_id = var.web_app_service_slot_app_service_plan_id
  tags                = var.environment_tags

  site_config {
    linux_fx_version = var.web_app_service_slot_application_framework
  }
  #   connection_string {
  #   name  = var.web_app_connection_string_name
  #   type  = var.web_app_connection_string_type
  #   value = var.web_app_connection_string_value
  # }


  app_settings = var.web_app_slot_settings
}
