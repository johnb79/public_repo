variable "web_app_service_slot_name" {} # staging
variable "web_app_service_slot_app_service_name" {}
variable "web_app_service_slot_location" {}
variable "web_app_service_slot_resource_group_name" {}
variable "web_app_service_slot_app_service_plan_id" {}
variable "web_app_service_slot_application_framework" {}
variable "environment_tags" {}

# variable "web_app_connection_string_name" {}
# variable "web_app_connection_string_type" {}
# variable "web_app_connection_string_value" {}

variable "web_app_slot_settings" { type = map }
