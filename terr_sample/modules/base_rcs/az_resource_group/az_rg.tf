resource "azurerm_resource_group" "rg_01" {
  name     = var.resource_group_name
  location = var.resource_group_location

  tags = var.environment_tags
}
