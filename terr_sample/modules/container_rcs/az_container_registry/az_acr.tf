
resource "azurerm_container_registry" "acr_01" {
  name                = var.container_registry_name
  resource_group_name = var.container_registry_rg_name
  location            = var.container_registry_location
  sku                 = var.container_registry_sku
  admin_enabled       = true
  tags                = var.environment_tags
}
