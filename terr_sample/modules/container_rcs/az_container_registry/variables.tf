variable "container_registry_name" {}
variable "container_registry_rg_name" {}
variable "container_registry_location" {}
variable "container_registry_sku" {}
variable "environment_tags" {}
