resource "azurerm_sql_database" "sql_database_01" {
  name                = var.sql_database_name
  resource_group_name = var.sql_database_resource_group_name
  location            = var.sql_database_location
  server_name         = var.sql_database_server_name

  tags = var.environment_tags
}
