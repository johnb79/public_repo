variable "sql_database_name" {}
variable "sql_database_resource_group_name" {}
variable "sql_database_location" {}
variable "sql_database_server_name" {}
variable "environment_tags" {}
