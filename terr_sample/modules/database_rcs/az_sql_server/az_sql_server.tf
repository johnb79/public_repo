# Create SQL server
resource "azurerm_sql_server" "sql_server_01" {
  name                         = var.sql_server_name
  resource_group_name          = var.sql_server_resource_group_name
  location                     = var.sql_server_location
  version                      = "12.0"
  administrator_login          = var.sql_server_admin_login
  administrator_login_password = var.sql_server_admin_password

  tags = var.environment_tags
}
