variable "sql_server_name" {}
variable "sql_server_resource_group_name" {}
variable "sql_server_location" {}
variable "sql_server_admin_login" {}
variable "sql_server_admin_password" {}
variable "environment_tags" {}
