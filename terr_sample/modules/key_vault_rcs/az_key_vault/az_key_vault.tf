
resource "azurerm_key_vault" "key_vault_01" {
  name                            = var.key_vault_name
  location                        = var.key_vault_location
  resource_group_name             = var.key_vault_resource_group_name
  enabled_for_disk_encryption     = true
  enabled_for_template_deployment = true
  enabled_for_deployment          = true
  tenant_id                       = var.key_vault_tenant_id

  sku_name = "standard"

  access_policy {
    tenant_id = var.key_vault_access_policy_tenant_id
    object_id = var.key_vault_access_policy_object_id

    key_permissions     = var.key_vault_access_policy_key_permissions
    secret_permissions  = var.key_vault_access_policy_secret_permissions
    storage_permissions = var.key_vault_access_policy_storage_permissions
  }

  network_acls {
    default_action = "Deny"
    bypass         = "AzureServices"
  }
  tags = var.environment_tags
}
