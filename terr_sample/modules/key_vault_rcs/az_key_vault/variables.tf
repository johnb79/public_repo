variable "key_vault_name" {}
variable "key_vault_location" {}
variable "key_vault_resource_group_name" {}
variable "key_vault_tenant_id" {}
variable "key_vault_access_policy_tenant_id" {}
variable "key_vault_access_policy_object_id" {}
variable "key_vault_access_policy_key_permissions" { type = list }
variable "key_vault_access_policy_secret_permissions" { type = list }
variable "key_vault_access_policy_storage_permissions" { type = list }
variable "environment_tags" {}
