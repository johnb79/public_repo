resource "azurerm_application_gateway" "network" {
  name                = var.application_gateway_name
  resource_group_name = var.application_gateway_resource_group_name
  location            = var.application_gateway_location
  tags                = var.environment_tags

  sku {
    name     = var.application_gateway_sku_name
    tier     = var.application_gateway_sku_tier
    capacity = var.application_gateway_sku_capacity
  }

  gateway_ip_configuration {
    name      = var.application_gateway_ip_configuration_name
    subnet_id = var.application_gateway_ip_configuration_subnet_id
  }

  frontend_port {
    name = var.application_gateway_frontend_port_name
    port = var.application_gateway_frontend_port_port
  }

  frontend_ip_configuration {
    name                 = var.application_gateway_frontend_ip_configuration_name
    public_ip_address_id = var.application_gateway_frontend_ip_configuration_public_ip_address_id
  }

  dynamic "backend_address_pool" {
    for_each = var.backend_address_pool
    content {
      name = backend_address_pool.value["name"]

    }
  }

  dynamic "backend_http_settings" {
    for_each = var.backend_http_settings
    content {
      name                                = backend_http_settings.value["name"]
      cookie_based_affinity               = backend_http_settings.value["cookie_based_affinity"]
      path                                = backend_http_settings.value["path"]
      port                                = backend_http_settings.value["port"]
      protocol                            = backend_http_settings.value["protocol"]
      request_timeout                     = backend_http_settings.value["request_timeout"]
      pick_host_name_from_backend_address = backend_http_settings.value["pick_host_name_from_backend_address"]
    }
  }

  dynamic "http_listener" {
    for_each = var.http_listener
    content {
      name                           = http_listener.value["name"]
      frontend_ip_configuration_name = http_listener.value["frontend_ip_configuration_name"]
      frontend_port_name             = http_listener.value["frontend_port_name"]
      protocol                       = http_listener.value["protocol"]
    }
  }

  dynamic "probe" {
    for_each = var.probe
    content {
      name                                      = probe.value["name"]
      protocol                                  = probe.value["protocol"]
      path                                      = probe.value["path"]
      interval                                  = probe.value["interval"]
      timeout                                   = probe.value["timeout"]
      unhealthy_threshold                       = probe.value["unhealthy_threshold"]
      pick_host_name_from_backend_http_settings = probe.value["pick_host_name_from_backend_http_settings"]

    }
  }
  # ssl_certificate {
  #   name     = var.application_gateway_ssl_certificate_name
  #   data     = var.application_gateway_ssl_certificate_data
  #   password = var.application_gateway_ssl_certificate_password
  # }

  # ssl_policy {
  #   policy_name = var.application_gateway_ssl_policy_name
  #   policy_type = var.application_gateway_ssl_policy_policy_type
  # }

  dynamic "request_routing_rule" {
    for_each = var.request_routing_rule
    content {
      name                       = request_routing_rule.value["name"]
      rule_type                  = request_routing_rule.value["rule_type"]
      http_listener_name         = request_routing_rule.value["http_listener_name"]
      backend_address_pool_name  = request_routing_rule.value["backend_address_pool_name"]
      backend_http_settings_name = request_routing_rule.value["backend_http_settings_name"]
    }
  }
  waf_configuration {
    enabled          = var.application_gateway_waf_configuration_enabled
    firewall_mode    = var.application_gateway_waf_configuration_firewall_mode
    rule_set_type    = var.application_gateway_waf_configuration_rule_set_type
    rule_set_version = var.application_gateway_waf_configuration_rule_set_version

  }

}
