# Application Gateway 
variable "application_gateway_name" {}
variable "application_gateway_resource_group_name" {}
variable "application_gateway_location" {}

# Application Gateway sku
# Application Gateway sku
variable "application_gateway_sku_name" {}
variable "application_gateway_sku_tier" {}
variable "application_gateway_sku_capacity" {}

# Application Gateway gateway_ip_configuration
variable "application_gateway_ip_configuration_name" {}
variable "application_gateway_ip_configuration_subnet_id" {}

# Application Gateway frontend_port
variable "application_gateway_frontend_port_name" {}
variable "application_gateway_frontend_port_port" {}

# Application Gateway frontend_ip_configuration
variable "application_gateway_frontend_ip_configuration_name" {}
variable "application_gateway_frontend_ip_configuration_public_ip_address_id" {}

# Application Gateway backend_address_pool
variable "backend_address_pool" {
  type = list(object({
    name = string

  }))
}

# Application Gateway backend_http_settings
variable "backend_http_settings" {
  type = list(object({
    name                                = string
    cookie_based_affinity               = string
    path                                = string
    port                                = number
    protocol                            = string
    request_timeout                     = number
    pick_host_name_from_backend_address = bool
  }))
}

# Application Gateway http_listener
variable "http_listener" {
  type = list(object({
    name                           = string
    frontend_ip_configuration_name = string
    frontend_port_name             = string
    protocol                       = string

  }))
}

# Application Gateway probe
variable "probe" {
  type = list(object({
    name                                      = string
    protocol                                  = string
    path                                      = string
    interval                                  = number
    timeout                                   = number
    unhealthy_threshold                       = number
    pick_host_name_from_backend_http_settings = bool
  }))
}


# Application Gateway redirect_configuration
variable "request_routing_rule" {
  type = list(object({
    name                       = string
    rule_type                  = string
    http_listener_name         = string
    backend_address_pool_name  = string
    backend_http_settings_name = string

  }))
}

# Application Gateway waf_configuration
variable "application_gateway_waf_configuration_enabled" {}
variable "application_gateway_waf_configuration_firewall_mode" {}
variable "application_gateway_waf_configuration_rule_set_type" {}
variable "application_gateway_waf_configuration_rule_set_version" {}


# Application Gateway rewrite_rule


variable "environment_tags" {}
