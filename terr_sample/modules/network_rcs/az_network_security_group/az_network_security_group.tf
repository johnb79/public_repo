

resource "azurerm_network_security_group" "security_group_01" {
  name                = var.network_security_group_name
  location            = var.network_security_group_location
  resource_group_name = var.network_security_group_resource_group_name
}


resource "azurerm_network_security_rule" "security_rule_01" {
  count                       = length(var.nsg_rules_01)
  direction                   = element(split(",", var.nsg_rules_01[count.index]), 0)
  priority                    = element(split(",", var.nsg_rules_01[count.index]), 1)
  name                        = element(split(",", var.nsg_rules_01[count.index]), 2)
  source_port_range           = element(split(",", var.nsg_rules_01[count.index]), 3)
  protocol                    = element(split(",", var.nsg_rules_01[count.index]), 4)
  source_address_prefix       = element(split(",", var.nsg_rules_01[count.index]), 5)
  destination_port_range      = element(split(",", var.nsg_rules_01[count.index]), 6)
  access                      = element(split(",", var.nsg_rules_01[count.index]), 7)
  destination_address_prefix  = "*"
  resource_group_name         = var.network_security_rule_resource_group_name
  network_security_group_name = azurerm_network_security_group.security_group_01.name
}


resource "azurerm_subnet_network_security_group_association" "security_group_01" {
  subnet_id                 = var.virtual_network_subnet_security_group_subnet_id
  network_security_group_id = var.virtual_network_subnet_network_security_group_id
}
