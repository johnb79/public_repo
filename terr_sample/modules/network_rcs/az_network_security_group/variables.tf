# Security Group Variables
variable "network_security_group_name" {}
variable "network_security_group_location" {}
variable "network_security_group_resource_group_name" {}

# Security Rule Variables
variable "network_security_rule_resource_group_name" {}
variable "nsg_rules_01" { type = map }

# Security Group Association Variables 
variable "virtual_network_subnet_security_group_subnet_id" {}
variable "virtual_network_subnet_network_security_group_id" {}
