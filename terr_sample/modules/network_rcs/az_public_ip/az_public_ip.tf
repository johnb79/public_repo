resource "azurerm_public_ip" "public_ip_01" {
  name                = var.public_ip_name
  resource_group_name = var.public_ip_resource_group_name
  location            = var.public_ip_location
  domain_name_label   = var.public_ip_domain_name_label
  allocation_method   = "Static"
  sku                 = "Standard"
  tags                = var.environment_tags
}
