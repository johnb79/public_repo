variable "public_ip_name" {}
variable "public_ip_resource_group_name" {}
variable "public_ip_location" {}
variable "public_ip_domain_name_label" {}
variable "environment_tags" {}
