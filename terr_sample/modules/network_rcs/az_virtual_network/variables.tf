variable "virtual_network_name" {}
variable "virtual_network_location" {}
variable "virtual_network_resource_group_name" {}
variable "virtual_network_address_space" { type = list }
variable "virtual_network_dns_servers" { type = list }
variable "environment_tags" {}
