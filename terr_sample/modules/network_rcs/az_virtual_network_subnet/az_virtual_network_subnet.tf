resource "azurerm_subnet" "virtual_network_subnet_01" {
  name                      = var.virtual_network_subnet_name
  resource_group_name       = var.virtual_network_subnet_resource_group_name
  virtual_network_name      = var.virtual_network_subnet_virtual_network_name
  address_prefix            = var.virtual_network_subnet_address_prefix
  network_security_group_id = var.virtual_network_subnet_network_security_group_association_id

}
