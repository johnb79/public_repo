variable "virtual_network_subnet_name" {}
variable "virtual_network_subnet_resource_group_name" {}
variable "virtual_network_subnet_virtual_network_name" {}
variable "virtual_network_subnet_address_prefix" {}
variable "virtual_network_subnet_network_security_group_association_id" {}
