resource "azurerm_storage_account" "storage_account_01" {
  name                     = var.storage_account_name
  resource_group_name      = var.storage_account_resource_group_name
  location                 = var.storage_account_location
  account_tier             = var.storage_account_tier
  account_replication_type = var.storage_account_rep_type

  tags = var.environment_tags
}

resource "azurerm_storage_container" "storage_container_01" {
  name                  = var.storage_container_name
  resource_group_name   = var.storage_account_resource_group_name
  storage_account_name  = azurerm_storage_account.storage_account_01.name
  container_access_type = var.storage_container_access_type
}