variable "storage_account_name" {}

variable "storage_account_location" {}

variable "storage_account_resource_group_name" {}

variable "storage_account_tier" {
    default = "Standard"
    description = "Storage Account Tier: Standard"
}

variable "storage_account_rep_type" {
    default = "LRS"
    description = "Storage Account Replication type: LRS, GRS"
}

variable "storage_container_name" {
    description = "Storage Container Name"
}

variable "storage_container_access_type" {
    default = "Private"
    description = "Storage Account Access type, Private, Public"
}

variable "environment_tags" {}


