resource "kubernetes_persistent_volume" "pers_storage_01" {
  metadata {
    name = "mysql-persistent-storage"
  }
  spec {
    capacity = {
      storage = "2Gi"
    }
    storage_class_name = "standard"
    access_modes       = ["ReadWriteMany"]
    persistent_volume_source {
      host_path {
        path = "/var/lib/mysql"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "pers_storage_01" {
  metadata {
    name = "mysql-persistent-storage"
  }
  spec {
    storage_class_name = "standard"
    access_modes       = ["ReadWriteMany"]
    resources {
      requests = {
        storage = "1Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.pers_storage_01.metadata.0.name
  }
}
