provider "kubernetes" {}


resource "kubernetes_service" "wordpress_service" {
  metadata {
    name = "wordpress"
    labels = {
      app = var.app_label
    }
  }
  spec {
    selector = {
      app  = var.app_label
      tier = var.wordpress_tier
    }

    port {
      port        = 80
      target_port = 80
      node_port   = 32500
    }

    type = "NodePort"
  }
}

resource "kubernetes_deployment" "wordpress_deployment" {
  metadata {
    name = "wordpress"
  }


  spec {
    replicas = 2

    selector {
      match_labels = {
        app  = var.app_label
        tier = var.wordpress_tier
      }
    }

    template {
      metadata {
        labels = {
          app  = var.app_label
          tier = var.wordpress_tier
        }

      }

      spec {
        container {
          name  = "wordpress"
          image = "wordpress:${var.wordpress_version}-apache"


          resources {
            limits {
              cpu    = "0.2"
              memory = "128Mi"
            }
            requests {
              cpu    = "0.1"
              memory = "64Mi"
            }
          }
          env {
            name  = "WORDPRESS_DB_HOST"
            value = "wordpress-mysql"
          }

          env {
            name  = "WORDPRESS_DB_PASSWORD"
            value = var.mysql_password
          }

          port {
            container_port = 80
            name           = "wordpress"
          }
        }

      }
    }
  }
}
