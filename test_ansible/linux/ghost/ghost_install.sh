#!/bin/bash

ip4=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)


cd /var/www/coolsite

GHOST_INSTALL=$(expect -c "
set timeout 60
spawn ghost install
expect \"Continue anyway?\"
send \"y\r\"
expect \"? Enter your blog URL:\"
send \"$ip4\r\"
expect \"? Enter your MySQL hostname:\"
send \"localhost\r\"
expect \"? Enter your MySQL username:\"
send \"ghost_coolsite_usr\r\"
expect \"? Enter your MySQL password:\"
send \"Qwer12345@#\r\"
expect \"? Enter your Ghost database name:\"
send \"ghost_coolsite_db\r\"
expect \"Do you wish to set up Nginx?\"
send \"y\r\"
expect \"Do you wish to set up Systemd?\"
send \"y\r\"
expect \"? Do you want to start Ghost? (Y/n)\"
send \"y\r\"
expect eof
")

echo "$GHOST_INSTALL"




