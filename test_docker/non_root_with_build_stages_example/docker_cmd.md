# Build an image from Alpine and add give it a name: tag (-t) myclient:1.0
docker build -t nonrootbuildstages:1.0 . 

# Run this image in a new container and access it.  The -it flag tells docker to run in interactive terminal mode, 
# and /bin/sh tells docker that it should run the shell
docker run -it nonrootbuildstages:1.0 /bin/sh

# User nodummy has read write access only on the specific folder(s). The user nodummy is not root