resource "google_compute_network" "staging_network" {
  project                 = "${var.gcp_project}"
  name                    = "${var.name}-network"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "staging_app_network" {
  name          = "${var.name}-subnet-app"
  project       = "${var.gcp_project}"
  ip_cidr_range = "10.2.0.0/16"
  region        = "${var.region}"
  network       = "${google_compute_network.staging_network.self_link}"
}

resource "google_compute_subnetwork" "staging_data_network" {
  name          = "${var.name}-subnet-data"
  project       = "${var.gcp_project}"
  ip_cidr_range = "10.3.0.0/16"
  region        = "${var.region}"
  network       = "${google_compute_network.staging_network.self_link}"
}

resource "google_compute_subnetwork" "staging_web_network" {
  name          = "${var.name}-subnet-web"
  project       = "${var.gcp_project}"
  ip_cidr_range = "10.1.0.0/16"
  region        = "${var.region}"
  network       = "${google_compute_network.staging_network.self_link}"
}

resource "google_compute_firewall" "rdp" {
  project = "${var.gcp_project}"
  name    = "windows-rdp"
  network = "${google_compute_network.staging_network.self_link}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["3389", "22"]
  }
}
