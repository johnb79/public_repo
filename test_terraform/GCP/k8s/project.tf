provider "google" {
  credentials = "${file("terr-vm-project.json")}"
  project     = "terr-vm-project"
  region      = "europe-west2"
}
