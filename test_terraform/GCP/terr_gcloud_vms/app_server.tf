resource "google_compute_instance" "app_server_instance" {
  count        = "${var.app_server_count}"
  project      = "${var.gcp_project}"
  name         = "${var.app_server_name}-${format("%02d", count.index)}"
  machine_type = "${var.app_server_size}"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "${var.app_server_vm_image}"
      type  = "${var.app_server_vm_os_disk_type}"
    }

    auto_delete = true
  }

  network_interface {
    # A default network is created for all GCP projects
    subnetwork_project = "${var.gcp_project}"
    subnetwork         = "${google_compute_subnetwork.staging_app_network.self_link}"
    access_config      = {}
  }
}

# To rdp over internet activate: Enable connecting to serial ports

