resource "google_compute_instance" "data_server_instance" {
  count        = "${var.data_server_count}"
  project      = "${var.gcp_project}"
  name         = "${var.data_server_name}-${format("%02d", count.index)}"
  machine_type = "${var.data_server_size}"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "${var.data_server_image}"
      type  = "${var.data_server_vm_os_disk_type}"
    }

    auto_delete = true
  }

  network_interface {
    # A default network is created for all GCP projects
    subnetwork_project = "${var.gcp_project}"
    subnetwork         = "${google_compute_subnetwork.staging_data_network.self_link}"
    access_config      = {}
  }
}

## To rdp over internet activate: Enable connecting to serial ports

