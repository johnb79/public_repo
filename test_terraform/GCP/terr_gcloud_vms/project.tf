provider "google" {
  credentials = "${file("terr-vm-project.json")}"
  project     = "terr-vm-project"
  region      = "europe-west2"
}

#  Create service account to alllow terraform to handle resources
# gcloud iam service-accounts create terr-vm
# gcloud projects add-iam-policy-binding terr-vm-project --member "serviceAccount:terr-vm@terr-vm-project.iam.gserviceaccount.com" --role "roles/owner"
#
# gcloud iam service-accounts keys create terr-vm-project.json --iam-account terr-vm@terr-vm-project.iam.gserviceaccount.com


## $env:GOOGLE_APPLICATION_CREDENTIALS="C:\Terraform\GCP\terr-vm-project.json"

