region = "europe-west2"

gcp_project = "terr-vm-project"

zone = "europe-west2-b"

# Network
name = "staging"

#Vm app-server
app_server_count = 2

app_server_name = "app-server"

app_server_size = "f1-micro"

app_server_vm_image = "debian-cloud/debian-9" #  windows-server-2019-dc-v20190411

app_server_vm_os_disk_type = "pd-standard"

#Vm data-server

data_server_count = 2

data_server_name = "data-server"

data_server_size = "f1-micro"

data_server_image = "debian-cloud/debian-9"

data_server_vm_os_disk_type = "pd-standard"

#Vm web-server

web_server_count = 2

web_server_name = "web-server"

web_server_size = "f1-micro"

web_server_image = "debian-cloud/debian-9"

web_server_vm_os_disk_type = "pd-standard"
