variable "region" {}
variable "gcp_project" {}
variable "zone" {}

#Network

variable "name" {}

#Vm app-server
variable "app_server_count" {}

variable "app_server_name" {}
variable "app_server_size" {}
variable "app_server_vm_image" {}
variable "app_server_vm_os_disk_type" {}

#Vm data-server
variable "data_server_count" {}

variable "data_server_name" {}
variable "data_server_size" {}
variable "data_server_image" {}
variable "data_server_vm_os_disk_type" {}

#Vm web-server
variable "web_server_count" {}

variable "web_server_name" {}
variable "web_server_size" {}
variable "web_server_image" {}
variable "web_server_vm_os_disk_type" {}
