module "resource_group" {
    source = "../modules/az_resource_group"

    env_prefix = var.ENV_PREFIX
    resource_group_name = var.RESOURCE_GROUP_NAME
    resource_group_location = var.RESOURCE_GROUP_LOCATION
}

module "azure_container_registry" {
  source = "../modules/az_acr"
  container_registry_name = var.CONTAINER_REGISTRY_NAME
  container_registry_resource_group_name = "${module.resource_group.resource_group_name}"
  container_registry_resource_group_location = "${module.resource_group.resource_group_location}"

}

module "azure_service_principal" {
  source = "../modules/az_service_principal"
  azuread_application_name = var.SERVICE_PRINCIPAL_DISPLAY_NAME

}


module "azure_aks" {
    source = "../modules/az_aks"

    # environment variables
    env_prefix = var.ENV_PREFIX
    resource_group_name = "${module.resource_group.resource_group_name}"

    # K8s vnet variables
    vnet_address_space = var.VNET_ADDRESS_SPACE

    # K8s vnet subnet variables
    subnet_name = var.SUBNET_NAME
    subnet_address_prefix = var.SUBNET_ADDRESS_PREFIX

    # K8s file share variables
    storage_account_name = var.STORAGE_ACCOUNT_NAME

    # K8s cluster variables
    k8s_cluster_name = var.K8S_CLUSTER_NAME
    k8s_cluster_location = "${module.resource_group.resource_group_location}"
    k8s_cluster_dns_prefix = var.K8S_CLUSTER_DNS_PREFIX
    k8s_cluster_tag = var.K8S_CLUSTER_TAG
    k8s_cluster_service_principal_client_id = "${module.azure_service_principal.sp_id}"
    k8s_cluster_service_principal_client_secret = "${module.azure_service_principal.sp_secret}"
    wait_for = module.azure_service_principal.id
}

module "azure_role_assignment" {
  source = "../modules/az_role_assignment"

role_assignment_principal_id = module.azure_service_principal.id
role_assignment_scope = module.azure_container_registry.acr_id
  
}