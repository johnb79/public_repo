# Setup TF_VAR_variable_name at azure devops variables for terraform to pick them up
# After setting TF_VAR_variable_name, remove values from vars. Example: variable "ENV_PREFIX" {}

#  Resource Group 
variable "ENV_PREFIX" {default = "dev"}
variable "RESOURCE_GROUP_NAME" {default = "k8s-cluster-rg"}
variable "RESOURCE_GROUP_LOCATION" {default = "uksouth"}

# Azure Container Registry
variable "CONTAINER_REGISTRY_NAME" {default = "johnbregistry"}

# Service Principal
variable "SERVICE_PRINCIPAL_DISPLAY_NAME" {default = "dev-sp-aks"}  

# Vnet, storage and K8s cluster
variable "VNET_ADDRESS_SPACE" {default = ["192.168.0.0/16"]}
variable "SUBNET_NAME" {default = "aks-subnet"}
variable "SUBNET_ADDRESS_PREFIX" {default = "192.168.1.0/24"}
variable "STORAGE_ACCOUNT_NAME" {default = "aksmainclusteruk"}
variable "K8S_CLUSTER_NAME" {default = "aks-main-cluster-uk"}
variable "K8S_CLUSTER_DNS_PREFIX" {default = "aks-first-cluster-uk"}
variable "K8S_CLUSTER_TAG" {default = "Development"}




