resource "azurerm_container_registry" "acr" {
  name                     = var.container_registry_name
  resource_group_name      = var.container_registry_resource_group_name
  location                 = var.container_registry_resource_group_location
  sku                      = "Basic"
  admin_enabled            = false
}
