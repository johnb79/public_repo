# Kubernetes Cluster

# Create the vnet for the cluster
resource "azurerm_virtual_network" "k8s" {
  name                = "${var.env_prefix}-${var.k8s_cluster_name}-vnet"
  location            = var.k8s_cluster_location
  resource_group_name = var.resource_group_name
  address_space       = var.vnet_address_space

  tags = {
    Environment = "${var.k8s_cluster_tag}-${var.k8s_cluster_name}"
  }
}

resource "azurerm_subnet" "k8s" {
  name                 = var.subnet_name
  resource_group_name  = var.resource_group_name
  address_prefix       = var.subnet_address_prefix
  virtual_network_name = azurerm_virtual_network.k8s.name

}

# Create the file share  for the cluster
resource "azurerm_storage_account" "file_share" {
  name                     = var.storage_account_name
  resource_group_name      = var.resource_group_name
  location                 = var.k8s_cluster_location
  account_tier             = "Standard"
  account_replication_type = "LRS"

    tags = {
    Environment = "${var.k8s_cluster_tag}-${var.k8s_cluster_name}"
  }
}

resource "azurerm_storage_share" "file_share" {
  name                 = "k8share"
  storage_account_name = azurerm_storage_account.file_share.name
  quota                = 50

}

# Create K8s Cluster
resource "azurerm_kubernetes_cluster" "k8s" {
  name                = "${var.env_prefix}-${var.k8s_cluster_name}"
  location            = var.k8s_cluster_location
  resource_group_name = var.resource_group_name
  dns_prefix          = var.k8s_cluster_dns_prefix

  default_node_pool {
    name                = "default"
    type                = "VirtualMachineScaleSets"
    enable_auto_scaling = true
    min_count           = 1
    max_count           = 4
    vm_size             = "Standard_A2"
    os_disk_size_gb     = 30

    vnet_subnet_id = azurerm_subnet.k8s.id

  }
  node_resource_group = "k8s-resources"

  service_principal {
    client_id     = var.k8s_cluster_service_principal_client_id
    client_secret = var.k8s_cluster_service_principal_client_secret
  }

  network_profile {
    network_plugin     = "kubenet"
    service_cidr       = "10.0.0.0/16"
    dns_service_ip     = "10.0.0.10"
    pod_cidr           = "10.244.0.0/16"
    docker_bridge_cidr = "172.17.0.1/16"
  }

  tags = {
    Environment = "${var.k8s_cluster_tag}-${var.k8s_cluster_name}"
  }
  depends_on = [var.wait_for]
}
