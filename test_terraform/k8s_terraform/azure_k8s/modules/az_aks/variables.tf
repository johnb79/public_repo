# environment variables
variable "env_prefix" {}
variable "resource_group_name" {}

# K8s vnet variables
variable "vnet_address_space" {} # list

# K8s vnet subnet variables
variable "subnet_name" {}
variable "subnet_address_prefix" {}

# K8s file share variables
variable "storage_account_name" {}

# K8s cluster variables
variable "k8s_cluster_name" {}
variable "k8s_cluster_location" {}
variable "k8s_cluster_dns_prefix" {}
variable "k8s_cluster_tag" {}
variable "k8s_cluster_service_principal_client_id" {}
variable "k8s_cluster_service_principal_client_secret" {}  # "${module.az_service_principal.sp_secret}"
variable "wait_for" {}