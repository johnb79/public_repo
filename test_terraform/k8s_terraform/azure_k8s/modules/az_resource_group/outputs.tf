output "resource_group_name" {
  value = "${azurerm_resource_group.k8s.name}"
}
output "resource_group_location" {
  value = "${azurerm_resource_group.k8s.location}"
}