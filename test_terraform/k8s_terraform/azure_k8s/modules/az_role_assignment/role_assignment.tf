


resource "azurerm_role_assignment" "role-assignment" {
  principal_id         = var.role_assignment_principal_id
  role_definition_name = "acrpull"
  scope                = var.role_assignment_scope
  
}