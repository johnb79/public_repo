resource "azuread_application" "service-principal-app" {
  name = var.azuread_application_name
}
resource "azuread_service_principal" "service-principal" {
  application_id = azuread_application.service-principal-app.application_id
}

resource "random_string" "service-principal-random-password" {
  length  = 16
  special = true

  keepers = {
    service_principal = "${azuread_service_principal.service-principal.id}"
  }
}

resource "azuread_service_principal_password" "service-principal-password" {
  service_principal_id = azuread_service_principal.service-principal.id
  value                = random_string.service-principal-random-password.result
  end_date             = "2020-01-01T01:02:03Z"


#   provisioner "local-exec" {
#     command     = "Start-Sleep -s 30"
#     interpreter = ["PowerShell"]
#   }
} 