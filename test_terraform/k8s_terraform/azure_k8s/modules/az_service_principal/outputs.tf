output "sp_id" {
  value = azuread_service_principal.service-principal.application_id
}

output "sp_secret" {
  value = random_string.service-principal-random-password.result
}

output "id" {
  value = azuread_service_principal.service-principal.id
}