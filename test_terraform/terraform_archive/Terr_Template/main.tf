variable "net_name" {
  default = "Testing-Network"
}

variable "app_srv_count" {
  description = " Changing the default value between 0 - 1 allows to recycle the vm. WARNING: 0 DELETES the VM AND the DATA DISKS ATTACHED PERMANTLY"
  default     = 1
}
variable "db_srv_count" {
  description = " Changing the default value between 0 - 1 allows to recycle the vm. WARNING: 0 DELETES the VM AND the DATA DISKS ATTACHED PERMANTLY"
  default     = 0
}

variable "web_srv_count" {
  description = " Changing the default value between 0 - 1 allows to recycle the vm. WARNING: 0 DELETES the VM AND the DATA DISKS ATTACHED PERMANTLY"
  default     = 0
}