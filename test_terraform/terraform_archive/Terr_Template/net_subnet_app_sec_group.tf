resource "azurerm_network_security_group" "app_sec_gr" {
  name                = "App_Subnet_SecurityGroup"
  location            = "${azurerm_resource_group.rg1.location}"
  resource_group_name = "${azurerm_resource_group.rg1.name}"
}
/*
resource "azurerm_network_security_rule" "app_subnet_rule" {
  count                       = "${length(var.rules_app)}"
  name                        = "${element(split(",",var.rules_app[count.index]),3)}"
  priority                    = "${element(split(",",var.rules_app[count.index]),4)}"
  direction                   = "${element(split(",",var.rules_app[count.index]),8)}"
  access                      = "${element(split(",",var.rules_app[count.index]),0)}"
  protocol                    = "${element(split(",",var.rules_app[count.index]),5)}"
  source_port_range           = "${element(split(",",var.rules_app[count.index]),7)}"
  destination_port_range      = "${element(split(",",var.rules_app[count.index]),2)}"
  source_address_prefix       = "${element(split(",",var.rules_app[count.index]),6)}"
  destination_address_prefix  = "${element(split(",",var.rules_app[count.index]),1)}"
  resource_group_name         = "${azurerm_resource_group.rg1.name}"
  network_security_group_name = "${azurerm_network_security_group.app_sec_gr.name}"
}

variable "rules_app" {
  description = "Exact copy of production nsg rules"
  type        = "map"

  default = {
    "0"  = "Allow,172.27.1.4/30,53,A-DNS FROM WEB TO AD,1000,*,172.27.0.0/24,*,Inbound"
    "1"  = "Allow,172.27.0.0/24,88,A-KERB FROM AD TO WEB,1015,*,172.27.1.4/30,*,Outbound"
  }
}
*/