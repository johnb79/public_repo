resource "azurerm_network_security_group" "data_sec_gr" {
  name                = "Data_Subnet_SecurityGroup"
  location            = "${azurerm_resource_group.rg1.location}"
  resource_group_name = "${azurerm_resource_group.rg1.name}"
}
/*
resource "azurerm_network_security_rule" "data_subnet_rule" {
  count                       = "${length(var.rules_data)}"
  name                        = "${element(split(",",var.rules_data[count.index]),3)}"
  priority                    = "${element(split(",",var.rules_data[count.index]),4)}"
  direction                   = "${element(split(",",var.rules_data[count.index]),8)}"
  access                      = "${element(split(",",var.rules_data[count.index]),0)}"
  protocol                    = "${element(split(",",var.rules_data[count.index]),5)}"
  source_port_range           = "${element(split(",",var.rules_data[count.index]),7)}"
  destination_port_range      = "${element(split(",",var.rules_data[count.index]),2)}"
  source_address_prefix       = "${element(split(",",var.rules_data[count.index]),6)}"
  destination_address_prefix  = "${element(split(",",var.rules_data[count.index]),1)}"
  resource_group_name         = "${azurerm_resource_group.rg1.name}"
  network_security_group_name = "${azurerm_network_security_group.data_sec_gr.name}"
}

variable "rules_data" {
  description = "Exact copy of production nsg rules"
  type        = "map"

  default = {
    "0" = "Allow,172.27.1.4/30,53,A-DNS FROM DATA TO AD,2000,*,172.27.2.0/24,*,Outbound"
    "1" = "Allow,172.27.1.4/30,88,A-KERB FROM DATA TO AD,2010,*,172.27.2.0/24,*,Outbound"
  }
}
*/