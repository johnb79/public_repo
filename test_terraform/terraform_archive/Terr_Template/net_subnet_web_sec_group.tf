resource "azurerm_network_security_group" "web_sec_gr" {
  name                = "Web_Subnet_SecurityGroup"
  location            = "${azurerm_resource_group.rg1.location}"
  resource_group_name = "${azurerm_resource_group.rg1.name}"
}
/*
resource "azurerm_network_security_rule" "web_subnet_rule" {
  count                       = "${length(var.rules_web)}"
  name                        = "${element(split(",",var.rules_web[count.index]),3)}"
  priority                    = "${element(split(",",var.rules_web[count.index]),4)}"
  direction                   = "${element(split(",",var.rules_web[count.index]),8)}"
  access                      = "${element(split(",",var.rules_web[count.index]),0)}"
  protocol                    = "${element(split(",",var.rules_web[count.index]),5)}"
  source_port_range           = "${element(split(",",var.rules_web[count.index]),7)}"
  destination_port_range      = "${element(split(",",var.rules_web[count.index]),2)}"
  source_address_prefix       = "${element(split(",",var.rules_web[count.index]),6)}"
  destination_address_prefix  = "${element(split(",",var.rules_web[count.index]),1)}"
  resource_group_name         = "${azurerm_resource_group.rg1.name}"
  network_security_group_name = "${azurerm_network_security_group.web_sec_gr.name}"
}

variable "rules_web" {
  description = "Exact copy of production nsg rules"
  type        = "map"

  default = {
    "0"  = "Allow,172.27.0.64/27,21,A-FTP FROM INTERNET TO WEB-PRO,900,TCP,INTERNET,*,Inbound"
    "1"  = "Allow,172.27.0.64/27,5000-5100,A-FTP-DYN FROM INTERNET TO WEB-PRO,910,TCP,INTERNET,*,Inbound"
  }
}
*/