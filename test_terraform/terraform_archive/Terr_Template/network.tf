resource "azurerm_resource_group" "rg1" {
  name     = "${var.net_name}"
  location = "northeurope"
}

resource "azurerm_virtual_network" "staging-network" {
  name                = "${var.net_name}"
  address_space       = ["172.27.0.0/16"]
  location            = "${azurerm_resource_group.rg1.location}"
  resource_group_name = "${azurerm_resource_group.rg1.name}"
  dns_servers         = ["172.27.0.4", "172.27.0.5"]
}

resource "azurerm_subnet" "staging-app-network" {
  name                      = "app"
  resource_group_name       = "${azurerm_resource_group.rg1.name}"
  virtual_network_name      = "${azurerm_virtual_network.staging-network.name}"
  address_prefix            = "172.27.1.0/24"
  network_security_group_id = "${azurerm_network_security_group.app_sec_gr.id}"
}

resource "azurerm_subnet" "staging-web-network" {
  name                      = "web"
  resource_group_name       = "${azurerm_resource_group.rg1.name}"
  virtual_network_name      = "${azurerm_virtual_network.staging-network.name}"
  address_prefix            = "172.27.0.0/24"
  network_security_group_id = "${azurerm_network_security_group.web_sec_gr.id}"
}

resource "azurerm_subnet" "staging-data-network" {
  name                      = "data"
  resource_group_name       = "${azurerm_resource_group.rg1.name}"
  virtual_network_name      = "${azurerm_virtual_network.staging-network.name}"
  address_prefix            = "172.27.2.0/24"
  network_security_group_id = "${azurerm_network_security_group.data_sec_gr.id}"
}

resource "azurerm_subnet" "gateway-subnet" {
  name                 = "GatewaySubnet"
  resource_group_name  = "${azurerm_resource_group.rg1.name}"
  virtual_network_name = "${azurerm_virtual_network.staging-network.name}"
  address_prefix       = "172.27.255.0/27"
}
