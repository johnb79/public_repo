resource "azurerm_resource_group" "rg3" {
  name     = "App_Servers"
  location = "northeurope"
}

resource "azurerm_availability_set" "abillap_av_set" {
  name                        = "APP_Availability_Set"
  location                    = "${azurerm_resource_group.rg3.location}"
  resource_group_name         = "${azurerm_resource_group.rg3.name}"
  managed                     = "false"
  platform_fault_domain_count = 2
}
