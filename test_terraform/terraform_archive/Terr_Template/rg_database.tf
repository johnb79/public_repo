resource "azurerm_resource_group" "rg5" {
  name     = "Database_Servers"
  location = "northeurope"
}

resource "azurerm_availability_set" "database_av_set" {
  name                        = "Database_Availability_Set"
  location                    = "${azurerm_resource_group.rg5.location}"
  resource_group_name         = "${azurerm_resource_group.rg5.name}"
  managed                     = "false"
  platform_fault_domain_count = 2
}