resource "azurerm_resource_group" "rg8" {
  name     = "Web_Servers"
  location = "northeurope"
}

resource "azurerm_availability_set" "portal_av_set" {
  name                        = "Portal_Availability_Set"
  location                    = "${azurerm_resource_group.rg8.location}"
  resource_group_name         = "${azurerm_resource_group.rg8.name}"
  managed                     = "false"
  platform_fault_domain_count = 2
}

resource "azurerm_public_ip" "lb2_ip" {
  name                         = "Public_IP_For_Portal"
  location                     = "${azurerm_resource_group.rg8.location}"
  resource_group_name          = "${azurerm_resource_group.rg8.name}"
  public_ip_address_allocation = "static"
}

resource "azurerm_lb" "lb2_lb" {
  name                = "Portal_External_Load_Balancer"
  location            = "${azurerm_resource_group.rg8.location}"
  resource_group_name = "${azurerm_resource_group.rg8.name}"

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = "${azurerm_public_ip.lb2_ip.id}"
  }
}

resource "azurerm_lb_backend_address_pool" "lb2_pool" {
  resource_group_name = "${azurerm_resource_group.rg8.name}"
  loadbalancer_id     = "${azurerm_lb.lb2_lb.id}"
  name                = "BackEndAddressPool"
}

resource "azurerm_lb_rule" "lb2_rule" {
  count                          = "${length(var.rules_lb_portal)}"
  resource_group_name            = "${azurerm_resource_group.rg8.name}"
  loadbalancer_id                = "${azurerm_lb.lb2_lb.id}"
  name                           = "${element(split(",",var.rules_lb_portal[count.index]),0)}"
  protocol                       = "${element(split(",",var.rules_lb_portal[count.index]),1)}"
  frontend_port                  = "${element(split(",",var.rules_lb_portal[count.index]),2)}"
  backend_port                   = "${element(split(",",var.rules_lb_portal[count.index]),3)}"
  frontend_ip_configuration_name = "${element(split(",",var.rules_lb_portal[count.index]),4)}"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.lb2_pool.id}"
  probe_id                       = "${azurerm_lb_probe.lb2_probe.id}"
  load_distribution              = "${element(split(",",var.rules_lb_portal[count.index]),5)}"
  idle_timeout_in_minutes        = "${element(split(",",var.rules_lb_portal[count.index]),6)}"
}

resource "azurerm_lb_probe" "lb2_probe" {
  resource_group_name = "${azurerm_resource_group.rg8.name}"
  loadbalancer_id     = "${azurerm_lb.lb2_lb.id}"
  name                = "Portal-Probe"
  port                = 80
  protocol            = "Tcp"
}

variable "rules_lb_portal" {
  description = "Standard set of predefined rules"
  type        = "map"

  default = {
    "0" = "lb_rule-PORTAL-80,Tcp,80,80,PublicIPAddress,SourceIP,30"
    "1" = "lb_rule-PORTAL-443,Tcp,443,443,PublicIPAddress,SourceIP,30"
  }
}