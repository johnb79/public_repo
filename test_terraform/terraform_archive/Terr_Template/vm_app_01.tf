variable "ap_name" {
  default = "appserver01"
}

variable "ap_ip" {
  default = "172.27.1.128"
}

variable "ap_size" {
  default = "Basic_A1"
}

resource "azurerm_network_interface" "app-network" {
  count                     = "${var.app_srv_count}"
  name                      = "${var.ap_name}_Nic"
  location                  = "${azurerm_resource_group.rg3.location}"
  resource_group_name       = "${azurerm_resource_group.rg3.name}"
  network_security_group_id = "${azurerm_network_security_group.app_sec_gr.id}"

  ip_configuration {
    name                          = "${var.ap_name}"
    subnet_id                     = "${azurerm_subnet.staging-app-network.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            = "${var.ap_ip}"
  }
}

locals {
  storage_account_app_uri = "${azurerm_storage_account.app.primary_blob_endpoint}${azurerm_storage_container.app_disks.name}"
}

resource "azurerm_storage_account" "app" {
  name                     = "${var.ap_name}storaccount"
  resource_group_name      = "${azurerm_resource_group.rg3.name}"
  location                 = "${azurerm_resource_group.rg3.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
  enable_file_encryption   = true
}

resource "azurerm_storage_container" "app_disks" {
  name                  = "${var.ap_name}vhds"
  resource_group_name   = "${azurerm_resource_group.rg3.name}"
  storage_account_name  = "${azurerm_storage_account.app.name}"
  container_access_type = "blob"
}

resource "azurerm_virtual_machine" "AP03_S" {
  count                 = "${var.app_srv_count}"
  name                  = "${var.ap_name}"
  location              = "${azurerm_resource_group.rg3.location}"
  resource_group_name   = "${azurerm_resource_group.rg3.name}"
  network_interface_ids = ["${element(azurerm_network_interface.app-network.*.id, count.index)}"]
  vm_size               = "${var.ap_size}"
  availability_set_id   = "${azurerm_availability_set.abillap_av_set.id}"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2012-R2-Datacenter"
    version   = "latest"
  }

  storage_os_disk {
    name          = "osdisk"
    caching       = "ReadWrite"
    create_option = "FromImage"
    vhd_uri       = "${local.storage_account_app_uri}/osdisk.vhd"
  }

  # Optional data disks

  storage_data_disk {
    name          = "datadisk1"
    vhd_uri       = "${local.storage_account_app_uri}/datadisk1.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 1
    disk_size_gb  = "1023"
  }
  storage_data_disk {
    name          = "datadisk2"
    vhd_uri       = "${local.storage_account_app_uri}/datadisk2.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 2
    disk_size_gb  = "1023"
  }
  /*
          storage_data_disk {
            name          = "datadisk3"
            vhd_uri       = "${local.storage_account_app_uri}/datadisk3.vhd"
            create_option = "Empty"
            caching       = "ReadOnly"
            lun           = 3
            disk_size_gb  = "1023"
          }
          storage_data_disk {
            name          = "datadisk4"
            vhd_uri       = "${local.storage_account_app_uri}/datadisk4.vhd"
            create_option = "Empty"
            caching       = "ReadOnly"
            lun           = 4
            disk_size_gb  = "1023"
          }
          */
  os_profile {
    computer_name  = "${var.ap_name}"
    admin_username = "Pink"
    admin_password = "Cheftest2018"
  }
  os_profile_windows_config {
    enable_automatic_upgrades = false
    provision_vm_agent        = true
  }
  tags {
    environment = "Staging"
  }
}
