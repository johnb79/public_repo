
variable "db_name" {
  default = "dbserver"
}

variable "db_ip" {
  default = "172.27.2.70"
}

variable "db_size" {
  default = "Basic_A2"
}

resource "azurerm_network_interface" "db-network" {
  count                     = "${var.db_srv_count}"
  name                      = "${var.db_name}_Nic"
  location                  = "${azurerm_resource_group.rg5.location}"
  resource_group_name       = "${azurerm_resource_group.rg5.name}"
  network_security_group_id = "${azurerm_network_security_group.data_sec_gr.id}"

  ip_configuration {
    name                          = "${var.db_name}"
    subnet_id                     = "${azurerm_subnet.staging-data-network.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            = "${var.db_ip}"
  }
}
locals {
  storage_account_db04_uri = "${azurerm_storage_account.db04.primary_blob_endpoint}${azurerm_storage_container.db04_disks.name}"
}

resource "azurerm_storage_account" "db04" {
  name                     = "${var.db_name}storaccount"
  resource_group_name      = "${azurerm_resource_group.rg5.name}"
  location                 = "${azurerm_resource_group.rg5.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "db04_disks" {
  name                  = "${var.db_name}vhds"
  resource_group_name   = "${azurerm_resource_group.rg5.name}"
  storage_account_name  = "${azurerm_storage_account.db04.name}"
  container_access_type = "private"
}

resource "azurerm_virtual_machine" "DB04_S" {
  count                 = "${var.db_srv_count}"
  name                  = "${var.db_name}"
  location              = "${azurerm_resource_group.rg5.location}"
  resource_group_name   = "${azurerm_resource_group.rg5.name}"
  network_interface_ids = ["${element(azurerm_network_interface.db-network.*.id, count.index)}"]
  vm_size               = "${var.db_size}"
  availability_set_id   = "${azurerm_availability_set.database_av_set.id}"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2012-R2-Datacenter"
    version   = "latest"
  }

  storage_os_disk {
    name          = "osdisk"
    caching       = "ReadWrite"
    create_option = "FromImage"
    vhd_uri       = "${local.storage_account_db04_uri}/osdisk.vhd"
  }

  # Optional data disks

  storage_data_disk {
    name          = "datadisk1"
    vhd_uri       = "${local.storage_account_db04_uri}/datadisk1.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 1
    disk_size_gb  = "1023"
  }
  storage_data_disk {
    name          = "datadisk2"
    vhd_uri       = "${local.storage_account_db04_uri}/datadisk2.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 2
    disk_size_gb  = "1023"
  }
  /*
  storage_data_disk {
    name          = "datadisk3"
    vhd_uri       = "${local.storage_account_db04_uri}/datadisk3.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 3
    disk_size_gb  = "1023"
  }
  storage_data_disk {
    name          = "datadisk4"
    vhd_uri       = "${local.storage_account_db04_uri}/datadisk4.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 4
    disk_size_gb  = "1023"
  }
  */
  os_profile {
    computer_name  = "${var.db_name}"
    admin_username = "Amber"
    admin_password = "Cheftest2018"
  }
  os_profile_windows_config {
    enable_automatic_upgrades = false
    provision_vm_agent        = true
  }
  tags {
    environment = "Staging"
  }
}
