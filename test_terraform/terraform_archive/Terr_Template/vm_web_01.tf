variable "web_name" {
  default = "webserver"
}

variable "web_ip" {
  default = "172.27.0.96"
}

variable "web_size" {
  default = "Standard_A1"
}

resource "azurerm_network_interface" "prl02-network" {
  count                     = "${var.web_srv_count}"
  name                      = "${var.web_name}_Nic"
  location                  = "${azurerm_resource_group.rg8.location}"
  resource_group_name       = "${azurerm_resource_group.rg8.name}"
  network_security_group_id = "${azurerm_network_security_group.web_sec_gr.id}"

  ip_configuration {
    name                                    = "${var.web_name}"
    subnet_id                               = "${azurerm_subnet.staging-web-network.id}"
    private_ip_address_allocation           = "Static"
    private_ip_address                      = "${var.web_ip}"
    load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.lb2_pool.id}"]
  }
}

locals {
  storage_account_prl02_uri = "${azurerm_storage_account.prl02.primary_blob_endpoint}${azurerm_storage_container.prl02_disks.name}"
}

resource "azurerm_storage_account" "prl02" {
  name                     = "${var.web_name}storaccount"
  resource_group_name      = "${azurerm_resource_group.rg8.name}"
  location                 = "${azurerm_resource_group.rg8.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "prl02_disks" {
  name                  = "${var.web_name}vhds"
  resource_group_name   = "${azurerm_resource_group.rg8.name}"
  storage_account_name  = "${azurerm_storage_account.prl02.name}"
  container_access_type = "private"
}

resource "azurerm_virtual_machine" "PRL02_S" {
  count                 = "${var.web_srv_count}"
  name                  = "${var.web_name}"
  location              = "${azurerm_resource_group.rg8.location}"
  resource_group_name   = "${azurerm_resource_group.rg8.name}"
  network_interface_ids = ["${element(azurerm_network_interface.prl02-network.*.id, count.index)}"]
  vm_size               = "${var.web_size}"
  availability_set_id   = "${azurerm_availability_set.portal_av_set.id}"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2012-R2-Datacenter"
    version   = "latest"
  }

  storage_os_disk {
    name          = "osdisk"
    caching       = "ReadWrite"
    create_option = "FromImage"
    vhd_uri       = "${local.storage_account_prl02_uri}/osdisk.vhd"
  }

  # Optional data disks

  storage_data_disk {
    name          = "datadisk1"
    vhd_uri       = "${local.storage_account_prl02_uri}/datadisk1.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 1
    disk_size_gb  = "1023"
  }
  storage_data_disk {
    name          = "datadisk2"
    vhd_uri       = "${local.storage_account_prl02_uri}/datadisk2.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 2
    disk_size_gb  = "1023"
  }
  /*
  storage_data_disk {
    name          = "datadisk3"
    vhd_uri       = "${local.storage_account_prl02_uri}/datadisk3.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 3
    disk_size_gb  = "1023"
  }
  storage_data_disk {
    name          = "datadisk4"
    vhd_uri       = "${local.storage_account_prl02_uri}/datadisk4.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 4
    disk_size_gb  = "1023"
  }
  */
  os_profile {
    computer_name  = "${var.web_name}"
    admin_username = "Beaver"
    admin_password = "Cheftest2018"
  }
  os_profile_windows_config {
    enable_automatic_upgrades = false
    provision_vm_agent        = true
  }
  tags {
    environment = "Staging"
  }
}
