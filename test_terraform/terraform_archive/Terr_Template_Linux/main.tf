variable "vnet_name" {
  default = "Staging-Network"
}

variable "vm_app_server_01_count" {
  default = 0
}

variable "vm_web_server_01_count" {
  default = 0
}

variable "vm_data_server_01_count" {
  default = 0
}
