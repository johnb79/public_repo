resource "azurerm_network_security_group" "web_net_sec_group" {
  name                = "Web_Subnet_SecurityGroup"
  location            = "${azurerm_resource_group.rg3.location}"
  resource_group_name = "${azurerm_resource_group.rg3.name}"
}

resource "azurerm_network_security_rule" "web_subnet_rule" {
  count                       = "${length(var.rules_web)}"
  access                      = "${element(split(",",var.rules_web[count.index]),0)}"
  destination_address_prefix  = "${element(split(",",var.rules_web[count.index]),1)}"
  destination_port_range      = "${element(split(",",var.rules_web[count.index]),2)}"
  name                        = "${element(split(",",var.rules_web[count.index]),3)}"
  priority                    = "${element(split(",",var.rules_web[count.index]),4)}"
  protocol                    = "${element(split(",",var.rules_web[count.index]),5)}"
  source_address_prefix       = "${element(split(",",var.rules_web[count.index]),6)}"
  source_port_range           = "${element(split(",",var.rules_web[count.index]),7)}"
  direction                   = "${element(split(",",var.rules_web[count.index]),8)}"
  resource_group_name         = "${azurerm_resource_group.rg3.name}"
  network_security_group_name = "${azurerm_network_security_group.web_net_sec_group.name}"
}

variable "rules_web" {
  description = "Create nsg rules"
  type        = "map"

  default = {
    "0" = "Allow,172.27.1.4/30,53,A-DNS FROM WEB TO AD,1000,*,172.27.0.0/24,*,Inbound"
    "1" = "Allow,172.27.0.0/24,88,A-KERB FROM AD TO WEB,1015,*,172.27.1.4/30,*,Outbound"
  }
}
