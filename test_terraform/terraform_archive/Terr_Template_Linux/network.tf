resource "azurerm_resource_group" "rg1" {
  name     = "${var.vnet_name}"
  location = "northeurope"
}

resource "azurerm_virtual_network" "vnet01_network" {
  name                = "${var.vnet_name}"
  address_space       = ["172.27.0.0/16"]
  location            = "${azurerm_resource_group.rg1.location}"
  resource_group_name = "${azurerm_resource_group.rg1.name}"
}

resource "azurerm_subnet" "vnet01_app_network" {
  name                      = "app"
  resource_group_name       = "${azurerm_resource_group.rg1.name}"
  virtual_network_name      = "${azurerm_virtual_network.vnet01_network.name}"
  address_prefix            = "172.27.1.0/24"
  network_security_group_id = "${azurerm_network_security_group.app_net_sec_group.id}"
}

resource "azurerm_subnet" "vnet01_web_network" {
  name                      = "web"
  resource_group_name       = "${azurerm_resource_group.rg1.name}"
  virtual_network_name      = "${azurerm_virtual_network.vnet01_network.name}"
  address_prefix            = "172.27.0.0/24"
  network_security_group_id = "${azurerm_network_security_group.web_net_sec_group.id}"
}

resource "azurerm_subnet" "vnet01_data_network" {
  name                      = "data"
  resource_group_name       = "${azurerm_resource_group.rg1.name}"
  virtual_network_name      = "${azurerm_virtual_network.vnet01_network.name}"
  address_prefix            = "172.27.2.0/24"
  network_security_group_id = "${azurerm_network_security_group.data_net_sec_group.id}"
}

resource "azurerm_subnet" "vnet01_gateway_subnet" {
  name                 = "GatewaySubnet"
  resource_group_name  = "${azurerm_resource_group.rg1.name}"
  virtual_network_name = "${azurerm_virtual_network.vnet01_network.name}"
  address_prefix       = "172.27.255.0/27"
}

# resource "azurerm_subnet_network_security_group_association" "app_subnet" {
#   subnet_id                 = "${azurerm_subnet.vnet01_app_network.id}"
#   network_security_group_id = "${azurerm_network_security_group.app_net_sec_group.id}"
# }
#
# resource "azurerm_subnet_network_security_group_association" "web_subnet" {
#   subnet_id                 = "${azurerm_subnet.vnet01_web_network.id}"
#   network_security_group_id = "${azurerm_network_security_group.web_net_sec_group.id}"
# }
#
# resource "azurerm_subnet_network_security_group_association" "data_subnet" {
#   subnet_id                 = "${azurerm_subnet.vnet01_data_network.id}"
#   network_security_group_id = "${azurerm_network_security_group.data_net_sec_group.id}"
# }

