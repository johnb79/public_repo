resource "azurerm_resource_group" "rg2" {
  name     = "Servers_App"
  location = "northeurope"
}

resource "azurerm_availability_set" "appserver_av_set" {
  name                        = "app_server_av_set"
  location                    = "${azurerm_resource_group.rg2.location}"
  resource_group_name         = "${azurerm_resource_group.rg2.name}"
  managed                     = "false"
  platform_fault_domain_count = 2
}
