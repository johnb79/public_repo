resource "azurerm_resource_group" "rg4" {
  name     = "Servers_Data"
  location = "northeurope"
}

resource "azurerm_availability_set" "dataserver_av_set" {
  name                        = "data_server_av_set"
  location                    = "${azurerm_resource_group.rg4.location}"
  resource_group_name         = "${azurerm_resource_group.rg4.name}"
  managed                     = "false"
  platform_fault_domain_count = 2
}
