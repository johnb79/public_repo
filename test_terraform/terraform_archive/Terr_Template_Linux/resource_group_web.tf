resource "azurerm_resource_group" "rg3" {
  name     = "Servers_Web"
  location = "northeurope"
}

resource "azurerm_availability_set" "webserver_av_set" {
  name                        = "web_server_av_set"
  location                    = "${azurerm_resource_group.rg3.location}"
  resource_group_name         = "${azurerm_resource_group.rg3.name}"
  managed                     = "false"
  platform_fault_domain_count = 2
}
