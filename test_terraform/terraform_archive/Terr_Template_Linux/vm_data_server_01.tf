variable "data_server_name" {
  default = "dataserver01"
}

variable "data_server_ip" {
  default = "172.27.2.128"
}

variable "data_server_size" {
  default = "Basic_A2"
}

resource "azurerm_public_ip" "data_ip" {
  name                = "${var.data_server_name}-PublicIP"
  location            = "${azurerm_resource_group.rg4.location}"
  resource_group_name = "${azurerm_resource_group.rg4.name}"
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "data01_nic" {
  count                     = "${var.vm_data_server_01_count}"
  name                      = "${var.data_server_name}_Nic"
  location                  = "${azurerm_resource_group.rg4.location}"
  resource_group_name       = "${azurerm_resource_group.rg4.name}"
  network_security_group_id = "${azurerm_network_security_group.data_net_sec_group.id}"

  ip_configuration {
    name                          = "${var.data_server_name}"
    subnet_id                     = "${azurerm_subnet.vnet01_data_network.id}"
    private_ip_address_allocation = "Static"
    private_ip_address            = "${var.data_server_ip}"
    public_ip_address_id          = "${azurerm_public_ip.data_ip.id}"
  }
}

locals {
  storage_account_data_uri = "${azurerm_storage_account.vm_data_server_01_storage_account.primary_blob_endpoint}${azurerm_storage_container.vm_data_server_01_vhds.name}"
}

resource "azurerm_storage_account" "vm_data_server_01_storage_account" {
  name                     = "${var.data_server_name}storaccount"
  resource_group_name      = "${azurerm_resource_group.rg4.name}"
  location                 = "${azurerm_resource_group.rg4.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
  enable_file_encryption   = true
}

resource "azurerm_storage_container" "vm_data_server_01_vhds" {
  name                  = "${var.data_server_name}vhds"
  resource_group_name   = "${azurerm_resource_group.rg4.name}"
  storage_account_name  = "${azurerm_storage_account.vm_data_server_01_storage_account.name}"
  container_access_type = "private"
}

resource "azurerm_virtual_machine" "vm_data_server_01" {
  count                 = "${var.vm_data_server_01_count}"
  name                  = "${var.data_server_name}"
  location              = "${azurerm_resource_group.rg4.location}"
  resource_group_name   = "${azurerm_resource_group.rg4.name}"
  network_interface_ids = ["${azurerm_network_interface.data01_nic.id}"]
  vm_size               = "${var.data_server_size}"
  availability_set_id   = "${azurerm_availability_set.dataserver_av_set.id}"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name          = "osdisk"
    caching       = "ReadWrite"
    create_option = "FromImage"
    vhd_uri       = "${local.storage_account_data_uri}/osdisk.vhd"
  }

  storage_data_disk {
    name          = "datadisk1"
    vhd_uri       = "${local.storage_account_data_uri}/datadisk1.vhd"
    create_option = "Empty"
    caching       = "ReadOnly"
    lun           = 1
    disk_size_gb  = "1023"
  }

  os_profile {
    computer_name  = "${var.data_server_name}"
    admin_username = "testadmin"
    admin_password = "Password1234!"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}
